import argparse
import ast

import os
import numpy as np
import torch
from torch.utils.data import DataLoader, sampler

import dataset
import utils as myutils
import models

# multiprocessing --> https://pytorch.org/docs/stable/data.html#single-and-multi-process-data-loading <--

if __name__ == '__main__':

    ####################
    ## Parser Section ##
    ####################

    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('--model', type=str,
                        help="The model name (a str). \n"
                        " e.g.: LSTM, MyCNN",
                        required=True)
    parser.add_argument('--window', type=int,
                        help="Window length (an int). \n"
                        " For CNN models, window_length is required. Value between [10,30] \n"
                        " e.g.: 30 .",
                        required=False,
                        default=None)
    parser.add_argument('--epoch', type=int,
                        help="Number of epochs (an int). \n"
                        " e.g.: 30 .",
                        required=False,
                        default=100)
    parser.add_argument('--engine', type=int,
                        help="Engine type (an int). \n"
                        " Available engine types are 1, 2, 3 or 4. \n"
                        " e.g.: 1 .",
                        required=False,
                        default=1)
    parser.add_argument('--batch', type=int,
                        help="Batch size (an int). \n"
                        " e.g.: 32 .",
                        required=False,
                        default=32)
    parser.add_argument('--transform', type=ast.literal_eval,
                        help="List of dataset transform names. \n"
                        " e.g.: '['MinMaxScaler']' .",
                        required=False,
                        default="[]")
    parser.add_argument('--bn',
                        help="Triggers batchnorm On.",
                        default=True, 
                        action='store_true')
    parser.add_argument('--no-bn', 
                        help="Triggers batchnorm Off.",
                        dest='bn', 
                        action='store_false')
    args = parser.parse_args()
    # print(vars(args))

    ################
    ## parameters ##
    ################

    dataset_dir = "Kaggle_Data"
    engine_type = args.engine
    model_name = args.model
    window_length = args.window

    validation_split = .3
    shuffle_dataset = False
    num_workers = os.cpu_count()
    num_epochs = args.epoch
    batch_size = args.batch
    transform_names = args.transform
    bn = args.bn

    device = torch.device(
        'cuda') if torch.cuda.is_available() else torch.device('cpu')

    mode, collate_fn = myutils.mode_selection(model_name=model_name)
    if (mode == "CNN") & (window_length is None):
        raise Exception(
            'For CNN or FC models, window_length is required. Use the option --window= .')

    #######################################
    ## Dataset and DataLoader definition ##
    #######################################

    transform = myutils.get_transform(transform_names=transform_names)
    trainval_dataset = dataset.NASA_CMAPS_Train_Dataset(
        dataset_dir=dataset_dir, engine_type=engine_type, mode=mode, transform=transform, window_length=window_length)
    test_dataset = dataset.NASA_CMAPS_Test_Dataset(
        dataset_dir=dataset_dir, engine_type=engine_type, mode=mode, transform=transform, window_length=window_length)

    trainval_size = len(trainval_dataset)
    test_size = len(test_dataset)

    # Creating data indices for training and validation splits
    indices = list(range(trainval_size))
    split = int(np.floor(validation_split * trainval_size))

    if shuffle_dataset:
        np.random.seed(42)  # random seeds = 42
        np.random.shuffle(indices)
    train_indices, val_indices = indices[split:], indices[:split]

    # Creating PT data samplers:
    train_sampler = sampler.SubsetRandomSampler(train_indices)
    valid_sampler = sampler.SubsetRandomSampler(val_indices)

    # Creating data loaders:
    train_loader = DataLoader(trainval_dataset, batch_size=batch_size, sampler=train_sampler,
                              num_workers=num_workers, collate_fn=collate_fn, pin_memory=True)
    valid_loader = DataLoader(trainval_dataset, batch_size=batch_size, sampler=valid_sampler,
                              num_workers=num_workers, collate_fn=collate_fn, pin_memory=True)
    test_loader = DataLoader(test_dataset, batch_size=batch_size,
                             num_workers=num_workers, collate_fn=collate_fn, pin_memory=True)

    print("The NASA_CMAPS train_loader has {} batches.".format(len(train_loader)))
    print("The NASA_CMAPS valid_loader has {} batches.".format(len(valid_loader)))
    print("The NASA_CMAPS test_loader has {} batches.".format(len(test_loader)))

    # batch = next(iter(train_loader))
    # print("Dataset input batches are of dimensions {} .".format(batch["time_series"].shape))

    ##########################
    ## Model initialisation ##
    ##########################

    model = myutils.initialize_model(
        model_name=model_name, device=device, window_length=window_length, bn=bn)

    #############################
    ## Training and validation ##
    #############################

    # 1/ training tools definition:
    criterion = torch.nn.MSELoss()
    optimizer = torch.optim.AdamW(params=model.parameters(), lr=1e-3)
    scheduler = torch.optim.lr_scheduler.StepLR(
        optimizer, step_size=10, gamma=0.1)
    # scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optimizer, mode='min', factor=0.1, patience=5, threshold=0.0001,
    #                                                       threshold_mode='rel', cooldown=0, min_lr=0, eps=1e-08, verbose=False)

    # 2/ training, validation and testing:
    best_modelpath = myutils.train_test_over_epoch(model_name, model, args, train_loader, valid_loader, test_loader,
                                                   num_epochs, criterion, optimizer, scheduler, device)

    ###########################
    ## Visualise predictions ##
    ###########################

    # 1/ apply best model to test dataset:
    # predictions, targets = myutils.predict(model, test_loader, device)

    # 2/ display results:
    # myutils.display_predictions(model_name, predictions, targets, show_graph=True)
