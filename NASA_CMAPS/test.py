import argparse

import os
import numpy as np
import torch
from torch.utils.data import DataLoader, sampler
from torchvision import transforms

import dataset
import utils as myutils
import models

def read_summary(summarypath):
    """ Generate and dump the summary of the model.
        summarypath   a pytorch model object
    """
    summary_file = open(summarypath)
    content = summary_file.read()
    print("Reading file: {}".format(summarypath))
    print(content)
    summary_file.close()

if __name__ == '__main__':

    ####################
    ## Parser Section ##
    ####################

    parser = argparse.ArgumentParser(formatter_class = argparse.RawTextHelpFormatter)
    parser.add_argument('--folder', type=str,
                        help="Path to model folder (a str). \n"
                        " e.g.: logs/engine_of_type_1/DeepCNN/DeepCNN_0 ",
                        required=True)
    args = parser.parse_args()

    #############################
    ## current test parameters ##
    #############################

    dataset_dir = "Kaggle_Data"

    path_to_folder = args.folder
    head, tail = os.path.split(path_to_folder)
    model_name = tail.split("_")[0]
    modelpath = path_to_folder + "/" + model_name + ".pt"
    summarypath = path_to_folder + "/summary.txt"
    commandpath = path_to_folder + "/command.pickle"
    
    read_summary(summarypath)

    #################################
    ## loading previous parameters ##
    #################################

    train_args = myutils.load_args(commandpath)

    engine_type = train_args.engine
    model_name = train_args.model
    window_length = train_args.window

    num_workers = os.cpu_count()
    num_epochs = train_args.epoch
    batch_size = train_args.batch
    transform_names = train_args.transform
    bn = train_args.bn

    device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')

    mode, collate_fn = myutils.mode_selection(model_name=model_name)
    
    ############################
    ## Retrieving information ##
    ############################

    transform = myutils.get_transform(transform_names=transform_names)
    test_dataset = dataset.NASA_CMAPS_Test_Dataset(dataset_dir=dataset_dir, engine_type=engine_type, mode=mode, transform=transform, window_length=window_length)
    test_loader = DataLoader(test_dataset, batch_size=batch_size, num_workers=num_workers, collate_fn=collate_fn, pin_memory=True)

    model = myutils.load_model(model_name, modelpath, window_length, device, bn)

    ###################################
    ## Run model and display results ##
    ###################################

    # 1/ apply model to test dataset:
    criterion = torch.nn.MSELoss()
    test_loss = myutils.test(model, test_loader, criterion, device)
    predictions, targets = myutils.predict(model, test_loader, device, sort=True)
    
    # 2/ display results:
    myutils.display_predictions(model_name, predictions, targets, show_graph=True, test_loss=test_loss)
