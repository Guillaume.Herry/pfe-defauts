import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.autograd import Variable

torch.manual_seed(1)

#######################
## Elementary Layers ##
#######################


def linear_relu_dropout(dim_in, dim_out, p_drop):
    return [nn.Linear(dim_in, dim_out),
            nn.ReLU(inplace=True),
            nn.Dropout(p_drop)]


def bn_dropout_linear_relu(dim_in, dim_out, p_drop):
    return [nn.BatchNorm1d(num_features=dim_in),
            nn.Dropout(p_drop),
            nn.Linear(dim_in, dim_out),
            nn.ReLU(inplace=True)]


def BatchNorm1d(num_features, bn=True):
    if bn:
        return nn.BatchNorm1d(num_features=num_features)
    else:
        return nn.Identity()


#############################
## RNN based architectures ##
#############################

class LSTM(nn.Module):

    def __init__(self,  input_size, hidden_size, nb_layers, dropout, bidirectional, device, bn=True):
        super(LSTM, self).__init__()

        self.nb_layers = nb_layers
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.dropout = dropout
        self.bidirectional = bidirectional
        self.device = device
        self.bn = bn

        self.lstm = nn.LSTM(input_size=self.input_size, hidden_size=self.hidden_size,
                            num_layers=self.nb_layers, batch_first=True, dropout=self.dropout,
                            bidirectional=self.bidirectional)

        self.output_size = self.hidden_size * \
            self.nb_layers*(2 if self.bidirectional else 1)

        self.fc_model = nn.Sequential(
            BatchNorm1d(num_features=self.output_size, bn=self.bn),
            nn.Dropout(p=0.5),
            nn.Linear(in_features=self.output_size, out_features=250),
            nn.ReLU(inplace=True),
            nn.Linear(250, 1)
        )

    def forward(self, x):

        _, (h_out, _) = self.lstm(x)
        h_out = h_out.view(-1, self.output_size)
        out = self.fc_model(h_out)

        return out.squeeze()

#############################
## CNN based architectures ##
#############################


class MyCNN(nn.Module):

    def __init__(self, nb_sensors, window_length, bn=True):
        super(MyCNN, self).__init__()

        self.nb_sensors = nb_sensors
        self.window_length = window_length
        self.bn = bn

        self.conv_layer = nn.Sequential(
            nn.Conv1d(in_channels=nb_sensors, out_channels=16,
                      kernel_size=3, stride=1, padding=0),
            nn.ReLU(inplace=True),
            nn.Conv1d(in_channels=16, out_channels=8,
                      kernel_size=3, stride=1, padding=0),
            nn.ReLU(inplace=True))

        # infer fc layer input size from conv layer output size:
        self.empty_tensor = torch.randn(1, self.nb_sensors, self.window_length)
        self.output_size = self.conv_layer(
            self.empty_tensor).shape[1]*self.conv_layer(self.empty_tensor).shape[2]

        self.fc_model = nn.Sequential(
            BatchNorm1d(num_features=self.output_size, bn=self.bn),
            nn.Dropout(p=0.5),
            nn.Linear(in_features=self.output_size, out_features=250),
            nn.ReLU(inplace=True),
            nn.Linear(250, 1)
            )

    def forward(self, inputs):
        conv_features = self.conv_layer(inputs)
        conv_features = conv_features.view(conv_features.size(0), -1)
        out = self.fc_model(conv_features)
        return out


class DeepCNN(nn.Module):

    def __init__(self, nb_sensors, window_length, bn=True, stride=1):
        super(DeepCNN, self).__init__()

        self.nb_sensors = nb_sensors
        self.window_length = window_length
        self.bn = bn
        self.stride = stride

        self.conv_layer = nn.Sequential(
            BatchNorm1d(num_features=nb_sensors, bn=self.bn),
            nn.Conv1d(in_channels=nb_sensors, out_channels=48,
                      kernel_size=3, stride=self.stride, padding=1),
            nn.Dropout(p=0.2),
            nn.ReLU(),
            BatchNorm1d(48, bn=self.bn),
            nn.Conv1d(in_channels=48, out_channels=32,
                      kernel_size=3, stride=self.stride, padding=1),
            nn.Dropout(p=0.2),
            nn.ReLU(),
            BatchNorm1d(32, bn=self.bn),
            nn.Conv1d(in_channels=32, out_channels=16,
                      kernel_size=3, stride=self.stride, padding=1),
            nn.Dropout(p=0.2),
            nn.ReLU(),
            BatchNorm1d(16, bn=self.bn),
            nn.Conv1d(in_channels=16, out_channels=8,
                      kernel_size=3, stride=self.stride, padding=1),
            nn.Dropout(p=0.2),
            nn.ReLU(),
            BatchNorm1d(8, bn=self.bn)
        )

        # infer fc layer input size from conv layer output size:
        self.empty_tensor = torch.randn(1, self.nb_sensors, self.window_length)
        self.output_size = self.conv_layer(
            self.empty_tensor).shape[1]*self.conv_layer(self.empty_tensor).shape[2]

        self.fc_model = nn.Sequential(
            BatchNorm1d(num_features=self.output_size, bn=self.bn),
            nn.Dropout(p=0.5),
            nn.Linear(in_features=self.output_size, out_features=250),
            nn.ReLU(inplace=True),
            nn.Linear(250, 1)
            )

    def forward(self, inputs):

        conv_features = self.conv_layer(inputs)
        conv_features = conv_features.view(conv_features.size(0), -1)
        out = self.fc_model(conv_features)
        return out


###################################
## Fully Connected architectures ##
###################################

class MyFC(nn.Module):

    def __init__(self, nb_sensors, window_length, bn=True):
        super(MyFC, self).__init__()

        self.nb_sensors = nb_sensors
        self.window_length = window_length
        self.input_size = self.nb_sensors * self.window_length
        self.bn = bn

        self.fc_model = nn.Sequential(
            BatchNorm1d(num_features=self.input_size, bn=self.bn),
            nn.Dropout(p=0.5),
            nn.Linear(in_features=self.input_size, out_features=250),
            nn.ReLU(inplace=True),
            BatchNorm1d(num_features=250, bn=self.bn),
            nn.Dropout(p=0.5),
            nn.Linear(in_features=250, out_features=500),
            nn.ReLU(inplace=True),
            nn.Linear(500, 1)
        )

    def forward(self, inputs):
        inputs_features = inputs.view(inputs.size(0), -1)
        out = self.fc_model(inputs_features)
        return out


#############################
## CNN & RNN architectures ##
#############################

class MixteModel(nn.Module):

    def __init__(self, nb_sensors, window_length, out_channel, device, bn=True):
        super(MixteModel, self).__init__()

        self.nb_layers = 1
        self.hidden_size = 1
        self.device = device
        self.bn = bn

        self.nb_sensors = nb_sensors
        self.window_length = window_length

        self.out_c1 = 18
        self.out_c2 = 36
        self.out_c3 = 72

        self.num_layer = 2

        self.out_channel = out_channel

        self.conv_layer1 = nn.Conv1d(
            in_channels=self.nb_sensors, out_channels=self.out_c1, kernel_size=2, stride=1, padding=0)
        self.conv_layer2 = nn.Conv1d(
            in_channels=self.out_c1, out_channels=self.out_c2, kernel_size=2, stride=1, padding=0)

        self.conv_layer3 = nn.Conv1d(
            in_channels=self.out_c2, out_channels=self.out_c3, kernel_size=2, stride=1, padding=0)

        self.relu = nn.ReLU()

        # self.fc1 = nn.Linear(48, 1)
        # self.fc2 = nn.Linear(self.nb_sensors*self.window_length, 30)

        self.pool = nn.MaxPool1d(
            2, stride=2, padding=0, dilation=1, return_indices=False, ceil_mode=False)

        self.fc1_model = nn.Sequential(
            *linear_relu_dropout(self.out_c3*2, self.nb_sensors*self.window_length, 0.2))

        self.lstm1 = nn.LSTM(input_size=self.nb_sensors,
                             hidden_size=self.nb_sensors*3,
                             num_layers=1,
                             batch_first=True,
                             bidirectional=False)
        # self.lstm2 = nn.LSTM(input_size=self.nb_sensors*3,
        #                     hidden_size=self.nb_sensors*3,
        #                     num_layers=1,
        #                     batch_first=True,
        #                     bidirectional=False)

        self.fc2_model = nn.Sequential(
            *linear_relu_dropout(self.nb_sensors*3, 50, 0.2))
        self.fc3 = nn.Linear(50, 1)

        self.conv_layer = nn.Sequential(self.conv_layer1,
                                        self.relu,
                                        self.pool,
                                        self.conv_layer2,
                                        self.relu,
                                        self.pool,
                                        self.conv_layer3,
                                        self.relu,
                                        self.pool)

        # infer fc layer input size from conv layer output size:
        # self.empty_tensor = torch.randn(1, self.nb_sensors, self.window_length)
        # self.output_size = self.conv_layer(
        #    self.empty_tensor).shape[1]*self.conv_layer(self.empty_tensor).shape[2]

    def forward(self, inputs):
        # print(self.conv_layer1.weight.shape)

        conv_out = self.conv_layer(inputs)
        # print(f'out conv before flatten shape {conv_out.shape}')
        conv_out = conv_out.view(conv_out.size(0), -1)
        # print('out conv shape',conv_out.shape)
        # print(self.fc1_model[0].weight.shape)
        out = self.fc1_model(conv_out)

        # print('out fc1 shape',out.shape)

        out = out.view(out.size(0), self.window_length, -1)
        # print('new out fc1 shape',out.shape)

        # h_0 = Variable(torch.zeros(self.nb_layers, out.size(0),
        #                           self.hidden_size)).to(self.device)
        # c_0 = Variable(torch.zeros(self.nb_layers, out.size(0),
        #                           self.hidden_size)).to(self.device)

        out_lstm1, (h_n, c_n) = self.lstm1(out)  # ,(h_0, c_0))
        # print('out lstm1 shape',out_lstm1.shape)
        # output, (h_n, c_n) = self.lstm2(out_lstm1, (h_1, c_1))
        h_n = h_n.view(-1, self.nb_sensors*3)
        # print('out_lstm1 shape',out_lstm1.shape)
        # out_lstm1=out_lstm1.view(32,-1)
        # print('hidden shape',h_n.shape)

        out = self.fc2_model(h_n)
        out = self.fc3(out)
        # out = self.fc_conv_test(conv_out)
        return out
