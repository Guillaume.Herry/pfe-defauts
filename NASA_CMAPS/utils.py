from tqdm import tqdm
import pickle
import numpy as np
from math import sqrt
import matplotlib.pyplot as plt
import torch
import os
import sys
import shutil
from torch.utils.tensorboard import SummaryWriter
from torchvision import transforms

import models
import dataset

################################
## initialize and load models ##
################################


def mode_selection(model_name):

    modes_dict = {"LSTM": ("RNN", custom_collate_fn), "MyCNN": ("CNN", None),
                  "MixteModel": ("CNN", None), "DeepCNN": ("CNN", None), "DeepCNNPool": ("CNN", None), "MyParaCNN": ("CNN", None),
                  "MyFC": ("CNN", None), "MyParaFC": ("CNN", None)}

    if model_name in modes_dict:
        mode, collate_fn = modes_dict[model_name]
    else:
        raise Exception(
            'Given model name {} does not correspond to an existing model. \n'
            'Available models are: {}'.format(
                model_name, [key for key in modes_dict.keys()])
        )

    return mode, collate_fn


def get_transform(transform_names):
    transform_dict = {"MinMaxScaler": dataset.MinMaxScalerVectorized(
    ), "Normalizer": dataset.Normalizer(), "ToTensor": dataset.ToTensor()}

    # by default ToTensor is the first transform to be applied:
    if "ToTensor" in transform_names:
        transform_list = []
    else:
        transform_list = [dataset.ToTensor()]

    # loop over given transform_names:
    for t_name in transform_names:
        if t_name in transform_dict:
            transform_list.append(transform_dict[t_name])
        else:
            raise Exception(
                "Given transform name '{}' does not correspond to an existing transform. \n"
                'Available transforms are: {}'.format(
                    t_name, [key for key in transform_dict.keys()])
            )

    return transforms.Compose(transform_list)


def initialize_model(model_name, device, window_length, bn):

    if model_name == "LSTM":
        model = models.LSTM(input_size=17, hidden_size=10,
                            nb_layers=3, dropout=0.5, bidirectional=True,
                            device=device, bn=bn)
    elif model_name == "MyCNN":
        model = models.MyCNN(nb_sensors=17, window_length=window_length, bn=bn)
    elif model_name == "MyParaCNN":
        model = models.MyParaCNN(
            nb_sensors=17, window_length=window_length, bn=bn)
    elif model_name == "DeepCNN":
        model = models.DeepCNN(
            nb_sensors=17, window_length=window_length, bn=bn)
    elif model_name == "DeepCNNPool":
        model = models.DeepCNNPool(
            nb_sensors=17, window_length=window_length, bn=bn)
    elif model_name == "MyFC":
        model = models.MyFC(nb_sensors=17, window_length=window_length, bn=bn)
    elif model_name == "MyParaFC":
        model = models.MyParaFC(
            nb_sensors=17, window_length=window_length, bn=bn)
    elif model_name == "MixteModel":
        model = models.MixteModel(
            nb_sensors=17, window_length=window_length, out_channel=24, device=device, bn=bn)
    else:
        raise Exception(
            'Model name given as argument does not correspond to an existing model')

    return model


def load_model(model_name, modelpath, window_length, device, bn):
    model = initialize_model(model_name, device, window_length, bn)
    model.load_state_dict(torch.load(modelpath))
    return model

####################
## train function ##
####################


def train(model, loader, optimizer, criterion, device):
    """
    Function to train a model on a given loader using the input optimizer
    @Inputs : 
    -- model : a torch.nn.Module instance
    -- loader : a torch.utils.data.DataLoader instance
    -- optimizer : a torch.optim object
    @Outputs :
    -- returns the average loss on the whole loader
    """

    model.train().to(device)

    total_loss, N = 0.0, 0
    for batch in tqdm(loader):

        inputs, labels = batch['time_series'].to(
            device), batch['RUL'].to(device)

        outputs = model(inputs.float())
        # print(f'input{inputs.shape}, label{labels.shape}', f'output {outputs.shape}')

        loss = criterion(outputs, labels.float())
        loss.backward()

        optimizer.step()
        optimizer.zero_grad()

        total_loss += loss * inputs.shape[0]
        N += inputs.shape[0]

    return total_loss/N

###################
## test function ##
###################


def test(model, loader, criterion, device):
    """
    Function to train a model on a given loader using the input optimizer
    @Inputs : 
    -- model : a torch.nn.Module instance
    -- loader : a torch.utils.data.DataLoader instance
    -- [Optional] optimizer : useless, to have the same signature than train()
    @Outputs :
    -- returns the average loss on the whole loader
    """

    model.eval().to(device)

    total_loss, N = 0.0, 0
    with torch.no_grad():
        for batch in tqdm(loader):

            inputs, labels = batch['time_series'].to(
                device), batch['RUL'].to(device)

            outputs = model(inputs.float())

            loss = criterion(outputs, labels.float())
            total_loss += loss * inputs.shape[0]
            N += inputs.shape[0]

    return total_loss/N

######################
## Model Checkpoint ##
######################


class ModelCheckpoint:
    """
    Class to manage to save logs and pre-trained Neural Network.
    @Methods : 
    -- __init__() : Define logs path
    -- update() : Save the current model if its results on the valid set are better than the saved one
    """

    def __init__(self, model_name, model, args, top_logdir="./logs"):
        self.min_loss = None
        self.model_name = model_name
        self.model = model
        self.args = args
        self.logdir = generate_logdir(top_logdir, self.model_name, args.engine)
        self.modelpath = self.logdir + "/" + self.model_name + ".pt"

        # save argparse args used during training:
        save_args(self.args, self.logdir)

        # Initialize tensorboard environnement:
        self.tensorboard_writer = SummaryWriter(log_dir=self.logdir)
        self.summary = model_summary(self.model, self.logdir)
        self.tensorboard_writer.add_text("Experiment summary", self.summary)

    def update(self, epoch, train_loss, val_loss):
        if (self.min_loss is None) or (val_loss < self.min_loss):
            print("Saving a better model")
            torch.save(self.model.state_dict(), self.modelpath)
            self.min_loss = val_loss

        self.tensorboard_writer.add_scalar(
            'metrics/train_loss', train_loss, epoch)
        self.tensorboard_writer.add_scalar(
            'metrics/valid_loss', val_loss, epoch)

#########################
## epoch loop function ##
#########################


def train_test_over_epoch(model_name, model, args, train_loader, validation_loader, test_loader,
                          num_epochs, criterion, optimizer, scheduler, device):
    """
    Function to train and test a model over epochs
    @Inputs : 
    -- model : a torch.nn.Module instance
    -- XX_loader : a torch.utils.data.DataLoader instance
    -- num_epochs : the number of epochs for your training
    -- model_checkpoint : a ModelCheckpoint instance
    @Outputs :
    -- Return nothing, the "model" object is modified globally
    """

    model_checkpoint = ModelCheckpoint(model_name, model, args)

    # A/ loop over epochs:
    for epoch in range(num_epochs):
        print(f"\n Epoch {epoch}")

        # 1/ training:
        train_loss = train(model, train_loader, optimizer, criterion, device)

        # 2/ validation:
        val_loss = test(model, validation_loader, criterion, device)
        print(
            f'Train loss: {train_loss:.4f} | Validation loss: {val_loss:.4f}')
        scheduler.step()

        # 3/ Model checkpoint:
        model_checkpoint.update(epoch, train_loss, val_loss)

    # B/ load best model and perform testing:
    model.load_state_dict(torch.load(model_checkpoint.modelpath))
    test_loss = test(model, test_loader, criterion, device)
    print(f'\n Test loss: {test_loss:.4f}')

    # C/ save results:
    predictions, targets = predict(model, test_loader, device, sort=True)
    display_predictions(model_name, predictions, targets,
                        logdir=model_checkpoint.logdir, test_loss=test_loss)

    return model_checkpoint.modelpath

######################
## predict function ##
######################


def predict(model, loader, device, output_attentions=False, sort=False):
    """
    Function to return the model prediction of a loader
    @Inputs : 
    -- model : a torch.nn.Module instance
    -- loader : a torch.utils.data.DataLoader instance
    -- [Optional] output_attentions : boolean to indicate if we want the model to return attention scores
    @Outputs :
    -- returns the output of the model
    """

    model.eval().to(device)

    results_list = []
    targets_list = []
    with torch.no_grad():
        for batch in loader:
            batch = {k: v.to(device) for k, v in batch.items()}

            inputs = batch["time_series"].float()
            targets = batch["RUL"].float()
            outputs = model(inputs)

            # transform tensor into lists:
            results_list += outputs.squeeze().tolist()
            targets_list += targets.squeeze().tolist()

    if len(results_list) != len(targets_list):
        raise Exception(
            'Number of RUL predictions different from number of RUL targets.')

    if sort:
        targets_list, results_list = zip(
            *sorted(zip(targets_list, results_list)))

    return results_list, targets_list


def display_predictions(model_name, predictions, targets, show_graph=False, logdir=None, test_loss=None):
    """ Displays predicted RUL against target RUL.
        If a log_dir is provided, saves the figure.
    """
    plt.figure()
    x = np.arange(1, len(predictions)+1)  # unit_id starts at 1.
    plt.plot(x, predictions, "-b", label="model predictions")
    plt.plot(x, targets, "-r", label="actual RUL")
    plt.legend(loc="upper left")
    plt.xlabel('unit_id')
    plt.ylabel('RUL')
    title_str = 'RUL predictions using "{}" model.'.format(model_name)
    if test_loss:
        title_str += '\nRMSE is {:0.1f} .'.format(sqrt(test_loss))
    plt.title(title_str)
    if show_graph:
        plt.show()
    if logdir:
        figpath = logdir + '/{}_plot.pdf'.format(model_name)
        plt.savefig(figpath, bbox_inches='tight')
    plt.close()

###############################
## Save model info functions ##
###############################


def generate_unique_logpath(logdir, raw_run_name):
    i = 0
    while(True):
        run_name = raw_run_name + "_" + str(i)
        log_path = os.path.join(logdir, run_name)
        if not os.path.isdir(log_path):
            return log_path
        i = i + 1


def generate_logdir(top_logdir, model_name, engine_type):
    """ Builds the following directory architecture:
        top_logdir/engine_of_type_{}/model_name/model_name_{}

        top_logdir  a str, name of the logs directory
        model_name  a str, name of the model
        engine_type an int, type of the engine (1,2,3 or 4)
    """
    # build the directory architecture:
    if not os.path.exists(top_logdir):
        os.mkdir(top_logdir)
    enginedir = os.path.join(
        top_logdir, "engine_of_type_{}".format(engine_type))
    if not os.path.exists(enginedir):
        os.mkdir(enginedir)
    modeldir = os.path.join(enginedir, model_name)
    if not os.path.exists(modeldir):
        os.mkdir(modeldir)

    # generate a unique directory name for the current run:
    logdir = generate_unique_logpath(modeldir, model_name)
    if not os.path.exists(logdir):
        os.mkdir(logdir)

    return logdir


def model_summary(model, logdir):
    """ Generate and dump the summary of the model.
        model   a pytorch model object
    """
    summary_file = open(logdir + "/summary.txt", 'w')
    summary_text = """
    Executed command
    ===============
    {}
    Dataset
    =======
    Model summary
    =============
    {}
    {} trainable parameters
    """.format(" ".join(sys.argv),
               str(model).replace('\n', '\n\t'),
               sum(p.numel() for p in model.parameters() if p.requires_grad)
               )
    summary_file.write(summary_text)
    summary_file.close()

    return summary_text


def save_args(args, logdir):
    with open(logdir + "/command.pickle", 'wb') as handle:
        pickle.dump(args, handle, protocol=pickle.HIGHEST_PROTOCOL)


def load_args(commandpath):
    with open(commandpath, 'rb') as handle:
        train_args = pickle.load(handle)
    return train_args

#######################
## custom collate_fn ##
#######################


def custom_collate_fn(batch):

    batch_size = len(batch)

    # 1/ Dealing with time_series
    time_series = [sample['time_series'] for sample in batch]
    nb_of_sensors = (time_series[0].shape)[1]
    time_series_lengths = [(time_serie.shape)[0] for time_serie in time_series]
    min_time_series_length = min(time_series_lengths)

    truncated_time_series = np.zeros(
        (batch_size, min_time_series_length, nb_of_sensors))
    for i, l_time_serie in enumerate(time_series_lengths):
        # truncate the begining of the longer sequences:
        # of length min_time_series_length
        truncated_time_series[i, :] = time_series[i][l_time_serie -
                                                     min_time_series_length:l_time_serie]

    batch_time_series = torch.tensor(truncated_time_series)

    # 2/ Dealing with RUL
    RULs = [sample['RUL'] for sample in batch]  # list of tensors
    batch_RUL = torch.cat(RULs, dim=0)

    return {'time_series': batch_time_series, 'RUL': batch_RUL}
