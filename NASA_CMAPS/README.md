## PFE-Defauts: The NASA_CMAPS dataset.

In this section we explore deeplearning approaches to tackle the problem of predictive maintance. We use the well-know *NASA_CMAPs* dataset to try and predict the RUL (remaining usefull life) of plane engines.

The Kaggle *NASA_CMAPs* dataset is available [here](https://www.kaggle.com/behrad3d/nasa-cmaps).

## Structure:

This section is organized into four modules:
- *dataset.py* gather custom functions to load the NASA_CMAPS dataset from the csv files located under *NASA_CMAP/Kaggle*.
- *models.py* implements our RNN and CNN pytorch models.
- *train.py* is our main code that trains our models.
- *test.py* loads a model and compute RUL predictions.
- *utils.py* gather useful functions.

## How to run:

- **For training, execute the following commands:**

    ```
    python3 train.py --model="LSTM"
    ````
    Note that for CNN and FC models, it is necessary to specify a window length with arg *--window* :

    ```
    python3 train.py --model="MyCNN" --window=31
    ```

    The following arguments are also available:
    - *--epoch*: number of epochs.
    - *--batch*: batch size.
    - *--engine*: the selected engine type (either 1, 2, 3 or 4).
    - *--transform*: list of transforms to be applied on the dataset.
    <br>
    Here is an exemple:  

    ```
    python3 train.py --model="DeepCNN" --window=31 --epoch=1 --batch=32 --engine=1 --transform="['MinMaxScaler']"
    ```

- **To read saved models, execute the following command:**

    Specify the folder where model informations are store using *--folder*:

    ```
    python3 test.py --folder="logs/engine_of_type_1/DeepCNN/DeepCNN_0"
    ```

