import warnings
import numpy as np
import pandas as pd

import torch
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms

import utils as myutils


def my_warning(message, category, filename, lineno, file=None, line=None):
    print("{}Warning: {} {}".format('\033[93m', message, '\033[0m'))


warnings.showwarning = my_warning

###################################
## Dataset description functions ##
###################################


def generate_dataset_description_train(self):
    """ Returns a description of the train dataset i.e. a list of triplets [unit, start cycle, end cycle]
        specifying how to retrive the information from data.

        for self.mode="CNN": end-start is constant and corresponds to self.windows_length if specified
                                or to the min unit_id length.
        for self.mode="RNN": end-start is either greater than self.windows_length if speficied
                                or greater than half of the current unit_id length.
    """
    L = []
    for i in range(self.nb_of_units):  # iterate over the units:
        unit_length = self.df_cycle_of_failure_train.iloc[i, 0]
        for start in range(unit_length):
            for end in range(start, unit_length):
                # out of all (start,end) combinations we only select the ones verifying the following test:
                if self.mode == "RNN":
                    if self.window_length:
                        def test(start, end): return (
                            self.window_length == (end-start+1))
                    else:
                        def test(start, end): return (
                            unit_length * 3/4 <= (end-start+1))
                else:  # self.mode is CNN:
                    if self.window_length:
                        def test(start, end): return (
                            (end-start+1) == self.window_length)
                    else:
                        def test(start, end): return ((end-start+1)
                                                      == self.min_cycle_of_failure_train)
                # applying the test:
                if test(start, end):
                    L.append([i+1, start, end])  # unit_id start at 1.
    return L


def generate_dataset_description_test(self):
    """ Returns a description of the test dataset i.e. a list of triplets [unit, start cycle, end cycle]
    specifying how to retrive the information from data.

    for self.mode="CNN": end-start is constant and corresponds to self.min_last_cycle_test.
    for self.mode="RNN": end-start covers the whole current unit_id.
    """
    L = []
    for i in range(self.nb_of_units):  # iterate over the units:
        unit_length = self.df_last_cycle_test.iloc[i, 0]
        if self.mode == "RNN":
            end = unit_length - 1
            if self.window_length:
                start = end - self.window_length + 1
            else:
                start = 0
        else:  # self.mode=="CNN":
            end = unit_length - 1
            if self.window_length:
                start = end - self.window_length + 1
            else:
                start = end - self.min_last_cycle_test + 1
        L.append([i+1, start, end])  # unit_id start at 1.
    return L

###############################################
## NASA_CMAPS Train Dataset class definition ##
###############################################


class NASA_CMAPS_Train_Dataset(Dataset):

    def __init__(self, dataset_dir: str, engine_type: int, mode: str, transform=None, window_length: int = None):
        """
        Args:
            dataset_dir (string): Path to the dataset directory.
            engine_type (int): id for the engine type, can be either 1, 2, 3 or 4.
            mode (string): either "RNN" or "CNN", specifies output dataset format.
            transform (callable, optional): Optional transform to be applied on a sample.
        """
        # integrate arguments as atttributes:
        self.dataset_dir = dataset_dir
        self.engine_type = engine_type
        self.mode = mode
        if self.mode not in ("CNN", "RNN"):
            raise TypeError('mode can either be "CNN" or "RNN".')
        self.window_length = window_length
        self.train_csv_dir = self.dataset_dir + \
            '/train_FD00' + str(self.engine_type) + '.txt'

        # read csv file as panda dataframe:
        self.df_train = pd.read_csv(
            self.train_csv_dir, delimiter=" ", header=None).dropna(axis=1, how='all')
        self.columns = ['unit_id', 'current_cycle', 'op_setting_1', 'op_setting_2', 'TRA', 'T2', 'T24', 'T30', 'T50', 'P2', 'P15', 'P30', 'Nf',
                        'Nc', 'epr', 'Ps30', 'phi', 'NRf', 'NRc', 'BPR', 'farB', 'htBleed', 'Nf_dmd', 'PCNfR_dmd', 'W31', 'W32']
        self.df_train.columns = self.columns
        self.nb_of_units = len(self.df_train.iloc[:, 0].unique())

        # Add columns "cycle_of_failure" and "RUL":
        self.df_cycle_of_failure_train = self.df_train[[
            'unit_id', 'current_cycle']].groupby(['unit_id']).max()
        self.df_train = self.df_train.merge(
            self.df_cycle_of_failure_train, how='left', left_on='unit_id', right_index=True)
        self.df_train.rename(columns={
                             "current_cycle_x": "current_cycle", "current_cycle_y": "cycle_of_failure"}, inplace=True)

        self.df_train['RUL'] = self.df_train['cycle_of_failure'] - \
            self.df_train['current_cycle']
        self.df_train['RUL'].clip(lower=0, inplace=True)

        # Calculates self.min_cycle_of_failure_train:
        self.min_cycle_of_failure_train = self.df_cycle_of_failure_train['current_cycle'].min(
        )
        if self.window_length:
            if self.min_cycle_of_failure_train < self.window_length:
                warnings.warn('Chosen window_length {} is greater than train shortest unit_id length {}.'.format(
                    self.window_length, self.min_cycle_of_failure_train))

        # Generates a dataset description, i.e. a list of triplets [unit, start cycle, end cycle]:
        self.train_dataset_description = generate_dataset_description_train(
            self)
        self.len_train_dataset_description = len(
            self.train_dataset_description)
        print("train_dataset generated with '{}' format: {} enteries.".format(
            self.mode, self.len_train_dataset_description), '\n')

        self.transform = transform

    def __len__(self):
        return self.len_train_dataset_description

    def get_nb_of_units(self):
        return self.nb_of_units

    def get_cycle_of_failure(self, unit_id: int):
        return self.df_cycle_of_failure_train.loc[unit_id].item()

    def __getitem__(self, idx):

        # reads the dataset description:
        unit_id, start, end = self.train_dataset_description[idx]

        # columns to be removed:
        constant_col_list = ['TRA', 'T2', 'P2',
                             'epr', 'farB', 'Nf_dmd', 'PCNfR_dmd']
        drop_cols_list = ['unit_id', 'current_cycle',
                          'cycle_of_failure', 'RUL'] + constant_col_list
        cols = [col for col in self.df_train.columns if col not in drop_cols_list]

        # extraction of the input sequence:
        df_unit_troncated = self.df_train[self.df_train['unit_id']
                                          == unit_id][cols][start:end+1]
        if self.mode == "RNN":
            time_series = df_unit_troncated.values.tolist()
        else:
            # transpose is needed to get (channels, space) order expected by the conv layers:
            time_series = np.transpose(df_unit_troncated.values)

        # extraction of the target RUL:
        RUL = [int(self.df_train[self.df_train['unit_id']
                                 == unit_id]['RUL'].iloc[end])]

        # definition of a sample:
        sample = {'time_series': time_series, 'RUL': RUL}

        if self.transform:
            sample = self.transform(sample)

        return sample

##############################################
## NASA_CMAPS Test Dataset class definition ##
##############################################


class NASA_CMAPS_Test_Dataset(Dataset):

    def __init__(self, dataset_dir: str, engine_type: int, mode: str, transform=None, window_length: int = None):
        """
        Args:
            dataset_dir (string): Path to the dataset directory.
            engine_type (int): id for the engine type, can be either 1, 2, 3 or 4.
            mode (string): either "RNN" or "CNN", specifies output dataset format.
            transform (callable, optional): Optional transform to be applied on a sample.
            window_length (int): specifies the window length. See generate_dataset_description function for more information.
        """

        # integrate arguments as atttributes:
        self.dataset_dir = dataset_dir
        self.engine_type = engine_type
        self.mode = mode
        if self.mode not in ("CNN", "RNN"):
            raise TypeError('mode can either be "CNN" or "RNN".')
        self.window_length = window_length
        self.test_csv_dir = self.dataset_dir + \
            '/test_FD00' + str(self.engine_type) + '.txt'
        self.RUL_csv_dir = self.dataset_dir + \
            '/RUL_FD00' + str(self.engine_type) + '.txt'

        # read csv file as panda dataframe:
        self.df_test = pd.read_csv(
            self.test_csv_dir, delimiter=" ", header=None).dropna(axis=1, how='all')
        self.df_RUL = pd.read_csv(
            self.RUL_csv_dir, delimiter=" ", header=None).dropna(axis=1, how='all')
        self.test_columns = ['unit_id', 'current_cycle', 'op_setting_1', 'op_setting_2', 'TRA', 'T2', 'T24', 'T30', 'T50', 'P2', 'P15', 'P30', 'Nf',
                             'Nc', 'epr', 'Ps30', 'phi', 'NRf', 'NRc', 'BPR', 'farB', 'htBleed', 'Nf_dmd', 'PCNfR_dmd', 'W31', 'W32']
        self.df_test.columns = self.test_columns
        self.df_RUL.columns = ['final_RUL']
        self.df_RUL.index += 1  # force index to start at 1, to match df_test indexing
        self.nb_of_units = len(self.df_test.iloc[:, 0].unique())

        # Itegrate dataframe df_RUL into dataframe df_test:
        self.df_last_cycle_test = self.df_test[['unit_id', 'current_cycle']].groupby([
                                                                                     'unit_id']).max()
        self.df_test = self.df_test.merge(
            self.df_RUL, how='left', left_on='unit_id', right_index=True)

        # Calculate min_last_cycle:
        self.min_last_cycle_test = self.df_last_cycle_test['current_cycle'].min(
        )
        if self.window_length:
            if self.min_last_cycle_test < self.window_length:
                raise TypeError('Chosen window_length {} is greater than shortest test unit_id length {}.'.format(
                    self.window_length, self.min_last_cycle_test))

        # Generates a dataset description, i.e. a list of triplets [unit, start cycle, end cycle]:
        self.test_dataset_description = generate_dataset_description_test(self)
        self.len_test_dataset_description = len(self.test_dataset_description)
        print("test_dataset generated with '{}' format: {} enteries.".format(
            self.mode, self.len_test_dataset_description), '\n')

        self.transform = transform

    def __len__(self):
        return self.len_test_dataset_description

    def get_nb_of_units(self):
        return self.nb_of_units

    def get_last_cycle(self, unit_id: int):
        return self.df_last_cycle_test.loc[unit_id].item()

    def get_cycle_of_failure(self, unit_id: int):
        return self.df_last_cycle_test.loc[unit_id].item() + self.df_RUL['final_RUL'][unit_id].item()

    def __getitem__(self, idx):

        # reads the dataset description:
        unit_id, start, end = self.test_dataset_description[idx]

        # columns to be removed:
        constant_col_list = ['TRA', 'T2', 'P2',
                             'epr', 'farB', 'Nf_dmd', 'PCNfR_dmd']
        drop_cols_list = ['unit_id', 'current_cycle',
                          'final_RUL'] + constant_col_list

        cols = [col for col in self.df_test.columns if col not in drop_cols_list]

        # extraction of the input sequence:
        df_unit_troncated = self.df_test[self.df_test['unit_id']
                                         == unit_id][cols][start:end+1]
        if self.mode == "RNN":
            time_series = df_unit_troncated.values.tolist()
        else:
            # transpose is needed to get (channels, space) order expected by the conv layers:
            time_series = np.transpose(df_unit_troncated.values)

        # extraction of the target RUL:
        RUL = [int(self.df_test[self.df_test['unit_id']
                                == unit_id]['final_RUL'].iloc[end])]

        # definition of a sample:
        sample = {'time_series': time_series, 'RUL': RUL}

        if self.transform:
            sample = self.transform(sample)

        return sample

###################################
## ToTensor transform definition ##
###################################


class ToTensor(object):
    """Convert ndarrays in sample to Tensors."""

    def __call__(self, sample):
        time_series, RUL = sample['time_series'], sample['RUL']
        return {'time_series': torch.Tensor(time_series), 'RUL': torch.tensor(RUL, dtype=torch.long)}


class Normalizer(object):
    """
    For each time_series set as a tensor, normalize each channel.
    """

    def __call__(self, sample):
        tensor, RUL = sample['time_series'], sample['RUL']
        mean, std = tensor.mean(dim=1), tensor.std(dim=1)
        tensor = tensor.permute(1, 0)
        std[std == 0.] = 1.
        scale = 1.0 / std
        tensor = tensor-mean
        tensor.mul_(scale)
        tensor = tensor.permute(1, 0)
        return {'time_series': tensor, 'RUL': RUL}


class MinMaxScalerVectorized(object):
    """
    For each time_series set as a tensor, convert each channel to the range [0, 1].
    """

    def __call__(self, sample):
        tensor, RUL = sample['time_series'], sample['RUL']
        dist = (tensor.max(dim=1, keepdim=True)[
            0] - tensor.min(dim=1, keepdim=True)[0])
        dist[dist == 0.] = 1.
        scale = 1.0 / dist
        tensor.mul_(scale).sub_(tensor.min(dim=1, keepdim=True)[0])
        return {'time_series': tensor, 'RUL': RUL}

##########################################
## Main code for checking above objects ##
##########################################


if __name__ == "__main__":

    ################
    ## parameters ##
    ################

    dataset_dir = "Kaggle_Data"

    ###################
    ## train dataset ##
    ###################

    """
    train_dataset = NASA_CMAPS_Train_Dataset(
        dataset_dir=dataset_dir, engine_type=1, mode="sequence", window_length=100)

    cycle_of_failure = train_dataset.get_cycle_of_failure(unit_id=1)
    print("cycle of failure for unit 1 is {}".format(cycle_of_failure))

    first_element = train_dataset[0]
    # print("the first dataset element is: {}".format(first_element))

    # testing that RUL and cyle_of_failure have coherent values:
    print("test: {}".format(cycle_of_failure == len(
        first_element['time_series'])+first_element['RUL'][0]), '\n')
    """

    ###################
    ## test dataset ##
    ###################

    """
    test_dataset = NASA_CMAPS_Test_Dataset(
        dataset_dir=dataset_dir, engine_type=1, mode="sequence")

    last_cycle = test_dataset.get_last_cycle(unit_id=1)
    print("last cycle for unit 1 is {}".format(last_cycle))

    first_element = test_dataset[0]
    # print("the first dataset element is: {}".format(first_element))

    # testing that last_cycle and length of time series have coherent values:
    print("test: {}".format(last_cycle == len(
        first_element['time_series'])), '\n')
    """

    ######################
    ## tensor transform ##
    ######################

    # transform = ToTensor()

    transform = transforms.Compose([ToTensor(), Normalizer()])

    train_dataset = NASA_CMAPS_Train_Dataset(
        dataset_dir=dataset_dir, engine_type=1, mode="CNN", transform=transform, window_length=30)
    test_dataset = NASA_CMAPS_Test_Dataset(
        dataset_dir=dataset_dir, engine_type=1, mode="CNN", transform=transform, window_length=30)

    print(train_dataset[0]['time_series'].shape)
    print(train_dataset[0]['RUL'])
    print(test_dataset[0])

    """
    print(train_dataset[0]['time_series'].shape)
    print(train_dataset[0]['RUL'])
    print(test_dataset[0])
    """
