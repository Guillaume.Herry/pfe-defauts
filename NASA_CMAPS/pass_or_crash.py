import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import torch

#########################
## Ploting experiments ##
#########################

"""
model_name = "MyCNN"

predictions = [int(x) for x in [1,2,3] ]
targets = [int(x) for x in [1.5,3,6] ]

plt.figure()
plt.plot(predictions, "-b", label="model predictions")
plt.plot(targets, "-r", label="actual RUL")
plt.legend(loc="upper left")
plt.xlabel('unit_id')
plt.ylabel('RUL')
plt.xticks(np.arange(0, len(predictions), step=1), np.arange(1, len(predictions)+1, step=1))
plt.title('RUL predictions using "{}" model.'.format(model_name))

#plt.savefig('{}_plot.pdf'.format(model_name), bbox_inches='tight')

plt.show()
"""

#####################
## pytorch permute ##
#####################

"""
a = torch.rand(4,4)
print(a)

for c in a.T: # transpose isn't a costly operation.
    print(c)

x = torch.randn(2, 3, 5)
print(x.size())
print(x.permute(1, 0, 2).size())

for col in x:
    print(col.shape)
    col = col.unsqueeze(dim=0)
    print(col.shape)
    """

#########################
## pytorch concatenate ##
#########################

"""
a = torch.rand(2,4)
b = torch.rand(2,4)

c = torch.cat([a,b],dim=1)

print(a)
print(b)
print(c)
"""
#####################
## pytorch flatten ##
#####################

"""
a = torch.rand(2,4)
print(a.flatten().shape)
"""

#######################
## conditional value ##
#######################

"""
test = True
a = (2 if test else 1)
print(a)
"""

###################
## sorting lists ##
###################

list1 = [3,2,4,1, 1]
list2 = ['three', 'two', 'four', 'one', 'one2']
list1, list2 = zip(*sorted(zip(list1, list2)))
print(list1,list2)