import numpy as np
import matplotlib.pyplot as plt
import ruptures as rpt

import signals as s

####################
## Exemple Modèle ##
####################

# generate signal
n_samples, dim, sigma = 1000, 3, 4
n_bkps = 4  # number of breakpoints
signal, bkps = rpt.pw_constant(n_samples, dim, n_bkps, noise_std=sigma)

# detection
algo = rpt.Pelt(model="rbf").fit(signal)
result = algo.predict(pen=10)

# display
rpt.display(signal, bkps, result)
plt.show()

#################################
## Utilisation sur nos signaux ##
#################################

# generate signal
t = np.linspace(-100, 100, 500)
defauts = [s.const(t),s.step_rectangle(t), s.ramp(t),s.intermittent(t)]
noise=s.noise_func(0, 0.10)
defauts_noise=[s.add_noise(t,y,noise) for y in defauts]

signal=defauts_noise[2]
# detection
algo = rpt.Pelt(model="rbf").fit(signal)
result = algo.predict(pen=10)

# display
rpt.display(signal, result)
plt.show()
