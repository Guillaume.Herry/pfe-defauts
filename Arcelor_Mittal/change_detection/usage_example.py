##@example usage_example.py
#This is an example of how to use the complete module:

import numpy as np
from tqdm import tqdm
import scipy.optimize as optim

import signals as s
import visualisation as visu
import change_detection as detect
import learning_detection_params as learning
    
t = np.linspace(-100, 100, 500)

# available detection methods and corresponding params_list (th,obs):
detection_list=[detect.mannwhitney_detection, detect.fligner_detection, detect.mood_detection]
params_list=[(0.1836881,42),(0.006,68),(0.005,65)]

# available signal_collections:
signal_collection_list=[[s.step_rectangle],[s.ramp],[s.noise_change]]

# for each signal_collection we want to test to detection method performance:
for signal_collection in signal_collection_list:
    
    # generate nb_samples signals from the chosen signal_collection
    X,Y=s.signal_samples(t,signal_collection,nb_samples=100)

    # run all available detection methods:
    prediction=learning.compute_prediction(t,X,detection_list,params_list)
    
    # display performance:
    visu.detection_plot(t,X,Y,prediction,"detection results")

    # mesure prediction error and performance statistics:
    pred_error = learning.prediction_error(Y,prediction)
    pred_stats = learning.prediction_stats(pred_error)
    
    # display statistics:
    learning.display_stats(detection_list, signal_collection, pred_stats, verbose=False)

# To go further, we can find the optimal detection parameters (th,obs):
learnt_params=learning.learn_parameters(t,nb_samples=100)
print(learnt_params)



