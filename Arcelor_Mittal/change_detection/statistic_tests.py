"""!@file statistic_tests.py
This module implements the following statistical tests:
    -> Mann-Whitney
    -> Mood
    -> Kolmogorov-Smirnov
    -> Bartlett
    -> Fligner-Killeen
"""

import math
import numpy as np
from matplotlib import pyplot as plt

import scipy.stats
from scipy.ndimage import uniform_filter1d

import nitime
import audiolazy

import signals as s
import visualisation as visu


class Test:

    def __init__(self, test_name, obs_length, max_p_value):

        self.disp_type = 1
        self.function = test_name


def moving_average(x, obs_length=10, max_p_value=1):
    ''' Returns the moving average calculated on moy_length'''
    return uniform_filter1d(x, size=obs_length)


def mannwhitney(x, incr=1, obs_length=20, max_p_value=1):
    ''' Test de Mann-Whitney appliqué au 1-D array x 

        Non parametric test

        Use only when the number of observation in each sample is > 20 
        and you have 2 independent samples of ranks. 
        Mann-Whitney U is significant if the u-obtained is LESS THAN 
        or equal to the critical value of U.
    '''
    obs_length = max(20, int(obs_length))
    i = 0
    res = np.ones((len(x), 2))
    while i*incr+2*obs_length+1 <= len(x):
        begin = i*incr
        a = x[begin:begin+obs_length]
        b = x[begin+obs_length+1:begin+2*obs_length+1]
        # The Mann-Whitney U statistic and the p-value:
        U, p = scipy.stats.mannwhitneyu(a, b, alternative='two-sided')
        if p < max_p_value:
            # constant sur une place de longueur incr
            res[begin+obs_length:begin+obs_length+incr] = [U, p]
        i += 1

    # constant padding:
    res[0:obs_length] = res[obs_length]
    res[len(x)-obs_length:len(x)] = res[len(x)-obs_length-1]

    return res


def mood(x, incr=1, obs_length=10, max_p_value=1):
    ''' Test de Mood appliqué au 1-D array x 

        Non parametric test

        Mood’s two-sample test for scale parameters is a non-parametric test 
        for the null hypothesis that two samples are drawn from the same distribution 
        with the same scale parameter.

    '''
    obs_length = int(obs_length)
    i = 0
    res = np.ones((len(x), 2))
    while i*incr+2*obs_length+1 <= len(x):
        begin = i*incr
        a = x[begin:begin+obs_length]
        b = x[begin+obs_length+1:begin+2*obs_length+1]
        # The z-score for the hypothesis test and the p-value:
        z, p = scipy.stats.mood(a, b)
        if p < max_p_value:
            res[begin+obs_length:begin+obs_length+incr] = [z, p]
        i += 1

    # constant padding:
    res[0:obs_length] = res[obs_length]
    res[len(x)-obs_length:len(x)] = res[len(x)-obs_length-1]

    return res


def ks(x, incr=1, obs_length=10, max_p_value=1):
    ''' Test de Kolmogorov-Smirnov appliqué au 1-D array x 

        Non parametric test

        This is a two-sided test for the null hypothesis 
        that 2 independent samples are drawn from the same continuous distribution. 

        The alternative hypothesis can be either ‘two-sided’ (default), ‘less’ or ‘greater’.
    '''
    obs_length = int(obs_length)
    i = 0
    res = np.ones((len(x), 2))
    while i*incr+2*obs_length+1 <= len(x):
        begin = i*incr
        a = x[begin:begin+obs_length]
        b = x[begin+obs_length+1:begin+2*obs_length+1]
        # KS test statistic (either D, D+ or D-) and the p-value (one sided or two sided):
        ks, p = scipy.stats.kstest(a, b)
        if p < max_p_value:
            res[begin+obs_length:begin+obs_length+incr] = [ks, p]
        i += 1

    # constant padding:
    res[0:obs_length] = res[obs_length]
    res[len(x)-obs_length:len(x)] = res[len(x)-obs_length-1]

    return res

# Note: le test statistique de Cramer-von-Mises sera introduit avec la version 1.6 de scipy


def bartlett(x, incr=1, obs_length=10, max_p_value=1):
    ''' Test de Bartlett appliqué au 1-D array x

        A parametric test for equality of k variances in normal samples

        Bartlett’s test tests the null hypothesis that all input samples 
        are from populations with equal variances. 
        For samples from significantly non-normal populations, 
        Levene’s test levene is more robust.
    '''
    obs_length = int(obs_length)
    i = 0
    res = np.ones((len(x), 2))
    while i*incr+2*obs_length+1 <= len(x):
        begin = i*incr
        a = x[begin:begin+obs_length]
        b = x[begin+obs_length+1:begin+2*obs_length+1]
        # The test statistic and the p-value:
        bar, p = scipy.stats.bartlett(a, b)
        if p < max_p_value:
            res[begin+obs_length:begin+obs_length+incr] = [bar, p]
        i += 1

    # constant padding:
    res[0:obs_length] = res[obs_length]
    res[len(x)-obs_length:len(x)] = res[len(x)-obs_length-1]

    return res


def fligner(x, incr=1, obs_length=10, max_p_value=1):
    ''' Perform Fligner-Killeen test for equality of variance.

        Non parametric test

        Fligner’s test tests the null hypothesis that 
        all input samples are from populations with equal variances. 
        Fligner-Killeen’s test is distribution free when populations are identical [2].
    '''
    obs_length = int(obs_length)
    i = 0
    res = np.ones((len(x), 2))
    while i*incr+2*obs_length+1 <= len(x):
        begin = i*incr
        a = x[begin:begin+obs_length]
        b = x[begin+obs_length+1:begin+2*obs_length+1]
        # The test statistic and the p-value:
        flg, p = scipy.stats.fligner(a, b)
        if p < max_p_value:
            res[begin+obs_length:begin+obs_length+incr] = [flg, p]
        i += 1

    # constant padding:
    res[0:obs_length] = res[obs_length]
    res[len(x)-obs_length:len(x)] = res[len(x)-obs_length-1]

    return res


def brandt(x, incr=1, obs_length=20):
    '''
        This function computes the AR modelisation on 3 differents moving windows

        Window A (First half of the window)
        Window B (Second half of the window)
        Window C (Full window)

        Then the Brandt Test is applied to the these three windows in order to calculate v
    '''
    obs_length = int(obs_length)
    i = 0
    res = np.ones((len(x), 2))
    while i*incr+2*obs_length+1 <= len(x):
        begin = i*incr

        # Creation of the three windows
        a = x[begin:begin+obs_length]
        b = x[begin+obs_length+1:begin+2*obs_length+1]
        c = x[begin:begin+2*obs_length+1]

        a = a-np.mean(a)
        b = b-np.mean(b)
        c = c-np.mean(c)

        # Simulation for the 3 windows
        [ak, sig_sq2] = nitime.algorithms.autoregressive.AR_est_LD(
            a, 2, rxx=None)
        [ak, sig_sq3] = nitime.algorithms.autoregressive.AR_est_LD(
            b, 2, rxx=None)
        [ak, sig_sq1] = nitime.algorithms.autoregressive.AR_est_LD(
            c, 2, rxx=None)

        N = len(a)

        # added abs to prevent error
        # print(sig_sq1, sig_sq2, sig_sq3)
        v = abs(2*N*np.log(math.sqrt(sig_sq1)) - N *
                np.log(math.sqrt(sig_sq2)) - N*np.log(math.sqrt(sig_sq3)))

        res[begin+obs_length:begin+obs_length+incr] = [v, None]
        i += 1

    # constant padding:
    res[0:obs_length] = res[obs_length]
    res[len(x)-obs_length:len(x)] = res[len(x)-obs_length-1]

    return res


if __name__ == '__main__':

    # 1/ Liste de défaults élémentaires (const, step, rampe, intermittent)
    t = np.linspace(-100, 100, 500)
    defauts = [s.noise_change(t), s.step_rectangle(t),
               s.ramp(t), s.intermittent(t)]

    # 2/ Buité par un bruit blanc:
    # 2.1/ Bruit blanc simple:
    noise = s.noise_func(0, 0.1)
    defauts_noise_simple = [s.add_noise(t, x, noise) for x in defauts]
    # 2.2/ Bruit blanc défini par morceaux:
    noise_pw = s.noise_pw_func(
        T=[len(t)//4], mu_sigma_list=[(0, 0.10), (0, 0.40)])
    defauts_noise_pw = [s.add_noise(t, x, noise_pw) for x in defauts]

    # 3/ Analyse de ces défaults élémentaires par les différents tests statistiques:
    # 3.1/ Tests sensibles à un changement de moyenne:
    tests_avg = [mannwhitney, ks]
    # 3.2/ Tests sensibles à un changement de variance:
    tests_var = [bartlett, fligner]
    # 3.3/ Tests sensibles à un changement d'echèle':
    tests_scale = [mood]
    # 3.4/ Tests basé sur le modèle AR:
    tests_AR = [brandt]

    # 4/ présentation des résultats:
    tests = tests_AR
    defauts_noise = defauts_noise_simple

    for test in tests:
        res = []
        for x in defauts_noise:
            # test parameters are obs_length and max_p_value:
            res += [test(x[0], incr=1, obs_length=50)]
        visu.test_plot(t, defauts_noise, res, test.__name__)

    # Rappel, la p-value est la probabilité de H0:
    # "les samples a et b sont issus de la même distribution".
    # Nous souhaitons repérer les zones de rupture où H0 n'est pas vérifiée,
    # donc les zones de très faible p-value très faible).
