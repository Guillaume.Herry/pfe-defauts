"""!@file signals.py
This module implements:
    -> basic signal functions (const, step_rectangel, ramp...)
    -> noise functions (noise_func, pw_noise_func,add_noise...)
    -> a function that generate a data stock of signals (signal_samples)
"""

import numpy as np
import random


class Signal:

    def __init__(self, f):

        self.disp_type = 0
        self.function = f

########################
# Défauts élémentaires #
########################


def const(t, const=0):
    ''' Returns a 1-D array of constant value
                a string of used parameters
    '''
    return np.zeros(t.shape), ""


def noise_change(t, t0=0):
    ''' Returns a 1-D array that simulates an increase of noise at t0 
                a string of used parameters (sig_0,sig_1) 
    '''
    y = np.zeros(len(t))
    sigma_0 = np.random.uniform(0.2, 0.4)
    sigma_1 = sigma_0+np.random.uniform(0.2, 0.4)
    params = "(sig_0={:.1f}, ".format(sigma_0) + \
        "sig_1={:.1f})".format(sigma_1)
    for i in range(len(t)):
        if t[i] < t0:
            y[i] = np.random.normal(0, sigma_0)
        else:
            y[i] = np.random.normal(0, sigma_1)
    return y, params


def step_rectangle(t, t0=0):
    ''' Returns a 1-D array of a step at t0 of initial value 0 and final value 1
                a string of used parameters
    '''
    return 1*(t >= t0), ""


def ramp(t, t0=0, slope=None, val_init=0, val_final=1):
    ''' Returns 1-D array of a ramp starting a t0
                a string of used parameters (slope=)

        val_init    -- initial value
        val_final   -- final value 
        t0          -- slope starting time point
        t1          -- slope ending time point
    '''
    if slope is None:
        slope = np.random.uniform(0.01, 2)
    params = "slope={:.2f}".format(slope)

    y = val_init + np.minimum(slope * np.maximum(t -
                                                 t0, 0.0), val_final - val_init)
    return y, params


def intermittent(t, t0=None, nb_step_min=10, nb_step_max=20):
    ''' Returns a random number of steps
                a string of used parameters (nb_step=)

        nb_step_min -- minimum number of steps in the chosen time interval
        nb_step_max -- maximum number of steps in the chosen time interval
    '''
    step = random.randint(nb_step_min, nb_step_max)
    params = "nb_step={}".format(step)

    step_list = random.sample(range(int(t[0]), int(t[-1])), step)
    res = sum(i > t for i in step_list) % 2

    return res, params

# #######################
# # Génération de bruit #
# #######################


def noise_func(mu=0, sigma=None):
    ''' Returns white noise function '''

    def noise(t, mu=mu, sigma=sigma):
        ''' Returns a 1-D white noise vector 
                    a string of used parameters (sig=)
        '''
        if sigma is None:
            sigma = np.random.uniform(0.2, 0.6)
        params = " sig={:.1f}".format(sigma)
        res = np.random.normal(mu, sigma, t.shape)
        return res, params

    return noise


def noise_pw_func(T, mu_sigma_list):
    ''' Returns piece-wise defined white noise vector function '''

    def noise_pw(t, T=T, mu_sigma_list=mu_sigma_list):
        ''' Returns a piece-wise defined normal noise function
                    a string of used parameters " pw_noise"

            T               -- list of incressing integers indexing normal distribution changes
            mu_sigma_list   -- list of normal parameters e.g.[(mu1,sigma1),(mu2,sigma2)]    
        '''
        if len(T)+1 != len(mu_sigma_list):
            raise NameError(
                "Size of T incompatible with size of mu_sigma_list")
        y = []
        t0_ini = 0
        for i in range(len(T)):
            t0 = int(T[i])
            mu, sigma = mu_sigma_list[i]
            y += np.random.normal(mu, sigma, t0-t0_ini).tolist()
            t0_ini = t0
        mu, sigma = mu_sigma_list[-1]
        y += np.random.normal(mu, sigma, int(len(t)-t0_ini)).tolist()
        return y, " pw_noise"

    return noise_pw


def add_noise(t, x, noise_func):
    ''' Returns an 1-D array corresponding to the sum of y and noise_func(t)
                a string of used parameters "x_params + noise_params"

        params:
        y           -- 1D array
        noise_func  -- noise function
    '''
    if len(t) != len(x[0]):
        raise NameError("Size of t incompatible with size of x")

    return x[0]+noise_func(t)[0], x[1]+noise_func(t)[1]

# #######################
# # Data Stock Creation #
# #######################


def signal_samples(t, signal_collection, nb_samples=1000, noise=None):
    """ returns X a 1-D array of samples x
                Y a 1_D array of targets y[i]=[(point_1,name_1)]

        signal_collection   -- list, of base signals
        nb_sample           -- int, number of samples to be drawn
        noise               -- float, representing the amount of noise
    """
    # generate a 1-D array of starting times t0:
    try:
        times = np.random.uniform(
            t[15], t[len(t)-15], nb_samples)  # limit edge effects
    except:
        times = np.random.uniform(t[0], t[-1], nb_samples)

    X = []
    Y = []
    for i in range(nb_samples):
        params = ""
        t0 = times[i]

        # randomly select a signal among the proposed collection:
        selected_signal = signal_collection[random.randint(
            0, len(signal_collection)-1)]

        # add noise only if selected_signal is not noise_change:
        if selected_signal.__name__ == "noise_change":
            res, param = selected_signal(t, t0=t0)
            X += [res]
            params += " "+param

        else:
            res, param = add_noise(t, selected_signal(
                t, t0=t0), noise_func(sigma=noise))
            X += [res]
            params += " "+param

        # save the name and time t0 of the default:
        Y += [[([t0], selected_signal.__name__ + params)]]

    return X, Y
