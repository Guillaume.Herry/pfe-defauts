# Changepoint detection folder

This folder contains all the files related to the changepoint detection 

* `visualisation.py`: This file contains the functions that are used to simplify the plot of the signal generated.

* `signals.py`: This file contains all the definition of the basic signals (ie: ramp, step_rectangle, intermittent...). It also contains the definition of the noise functions added to the basic signals to generate noise.

* `statistic_tests.py`: This file contains the functions of the statiscal tests that we want to apply to the others.
  
* `learning_detection_params.py`: This file is used to learn the parameters used in the detection files. It is based on several functions that are optimizing the parameters values by computing their prediction error. 

* `ruptures_package_test.py`: This file import the ruptures package and apply it to basic signals in order to do some test cases of the package. 

   
  

