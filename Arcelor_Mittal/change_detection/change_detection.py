"""!@file change_detection.py
This module implements change point detection methods using the statistical test outputs:
    -> extrema detection methods
    -> threshold detection methods
    -> p-value detection methods

This module also implements pre-designed test + detection functions
which use a statistical test and the appropriate detection method:
    -> mannwhitney_detection
    -> mood_detection
    -> fligner_detection
"""

import numpy as np
from scipy.signal import argrelextrema
from tqdm import tqdm

import signals as s
import visualisation as visu
import statistic_tests as test_stat


def extrema_detection(t, test_output, th=0.001):
    ''' returns a 1-D array of change point times according to
                the extrema detection method (filter on p-value
                followed by a global extrema detection)

        test_output -- a 2-D array corresponding to the statistical test output
        th          -- float corresponding to the threshold
    '''
    v = test_output[:, 0]
    p = test_output[:, 1]

    # only study v on areas where p is below threshold th:
    th_mask = (p < th)
    # list of indices where p crosses the threshold
    indices = np.nonzero(np.diff(th_mask))[0] + 1

    t_bits = np.split(t, indices)  # splits t according to indices
    v_bits = np.split(v, indices)  # splits v according to indices
    # keep in memory whether bits are below or above th
    mask_bits = np.split(th_mask, indices)

    min_points = []
    max_points = []
    for t_bit, v_bit, mask_bit in zip(t_bits, v_bits, mask_bits):
        if (1 <= len(v_bit)) & mask_bit.all():
            min_points += [t_bit[np.argmin(v_bit)]]
            max_points += [t_bit[np.argmax(v_bit)]]

    return min_points, max_points


def max_detection(t, test_output, th=0.001):
    ''' returns a 1-D array of change point times according to
                the extrema detection method (filter on p-value
                followed by a global extrema detection)

        test_output -- a 2-D array corresponding to the statistical test output
        th          -- float corresponding to the threshold
    '''
    v = test_output[:, 0]
    p = test_output[:, 1]

    # only study v on areas where p is below threshold th:
    th_mask = (th < p)
    # list of indices where p crosses the threshold
    indices = np.nonzero(np.diff(th_mask))[0] + 1

    print('indices', indices)

    t_bits = np.split(t, indices)  # splits t according to indices

    print('t_bits', t_bits)
    v_bits = np.split(v, indices)  # splits v according to indices
    # keep in memory whether bits are below or above th
    mask_bits = np.split(th_mask, indices)
    # print(mask_bits)
    #min_points = []
    max_points = []
    print(len(t_bits))
    i = 0
    for t_bit, v_bit, mask_bit in zip(t_bits, v_bits, mask_bits):
        i += 1
        print(i)
        print('all', mask_bit.all())
        print('len_v ', len(v_bit))
        if (1 <= len(v_bit)) & mask_bit.all():
            print('inner', i)

            #print('min , max', np.argmin(v_bit), np.argmax(v_bit))
            #min_points += [t_bit[np.argmin(v_bit)]]
            max_points += [t_bit[np.argmax(v_bit)]]

    return max_points


def min_detection(t, test_output, th=0.001):
    return extrema_detection(t, test_output, th=th)[0]


def max_detection(t, test_output, th=0.001):
    return extrema_detection(t, test_output, th=th)[1]


def threshold_detection(t, test_output, th=0.25, smoothing=None):
    ''' returns a 1-D array of change point times indexes

        test        -- 2-D array corresponding to the statistical test output
        th          -- float corresponding to the threshold
        smoothing   -- int corresponding to smoothing length
    '''
    v = test_output[:, 0]
    p = test_output[:, 1]
    if smoothing != None:
        v = test_stat.moving_average(v, obs_length=smoothing)

    # on normalise chaque entrée du vecteur v
    absmax = max(abs(v))
    if absmax != 0:
        v = v/absmax

    # boolean array of size len(v)-1. Term i is True if and only if v passes the threshold:
    mask = np.diff((v < th))

    points = []
    for i in range(len(t)-1):
        if (p[i] < 0.001) & mask[i]:
            points += [t[i]]

    return points


def pvalue_detection(t, test_output, th=0.001):
    ''' returns a 1-D array of change point times indexes

        test    -- a 2-D array corresponding to the statistical test output
        th      -- a float corresponding to the threshold
    '''
    v = test_output[:, 0]
    p = test_output[:, 1]

    # Boolean array of size len(p)-1. Term i is True if and only if p passes the threshold:
    mask = np.diff((p < th))
    points = t[:-1][mask]

    return points


def select_test_and_detection(test, detection):
    ''' returns a test_detection function

        test    -- selected test function name
        th      -- selected detection function name
    '''
    def generic_test_detection(t, signal, th, obs_length=50):
        ''' returns a 1-D array of change point time indexes
                    a string name of the used method: test and detection

            signal      -- a 1-D array representing the signal function
            th          -- a float corresponding to the detection threshold
            obs_length  -- an int corresponding to the sample length used in test
        '''
        test_res = test(signal, obs_length=obs_length)
        title = test.__name__+" & "+detection.__name__
        return detection(t, test_res, th=th), title

    return generic_test_detection


def mannwhitney_detection(t, signal, th=0.2, obs_length=50):
    f = select_test_and_detection(test_stat.mannwhitney, threshold_detection)
    return f(t, signal, th=th, obs_length=obs_length)


def mood_detection(t, signal, th=0.003, obs_length=50):
    f = select_test_and_detection(test_stat.mood, min_detection)
    return f(t, signal, th=th, obs_length=obs_length)


def fligner_detection(t, signal, th=0.002, obs_length=50):
    f = select_test_and_detection(test_stat.fligner, max_detection)
    return f(t, signal, th=th, obs_length=obs_length)


if __name__ == '__main__':

    # ##################
    # # Manual Testing #
    # ##################

    # 1/ Liste de défaults élémentaires (noise, step, rampe)
    #    bruités par un bruit blanc:
    t = np.linspace(-100, 100, 500)
    defauts = [s.step_rectangle(t), s.ramp(t)]
    noise = s.noise_func(0, 0.4)
    defauts_noise = [s.add_noise(t, x, noise) for x in defauts]

    # 2/ Sélection du test et de la méthode de détection:
    test = test_stat.mannwhitney
    detection = min_detection

    for x in defauts_noise:

        res = test(x[0], obs_length=50)
        points = detection(t, res)

        print_output = [(2, (x[0], [(points, "")]))]
        # visu.functions_plot(t,print_output,test.__name__+" & "+detection.__name__)

    # #####################
    # # Automatic Testing #
    # #####################

    # 1/ automatic generation of signals:
    signal_collection = [s.noise_change, s.step_rectangle, s.ramp]
    noise = 0.5
    X, Y = s.signal_samples(t, signal_collection, noise=noise, nb_samples=10)
    # visu.detection_plot(t,X,Y,Y,"my signal samples (sigma={})".format(noise))

    # 2/ pre-made detection methods:
    prediction = []
    for x in tqdm(X):
        test_preds = []
        for detect in [mannwhitney_detection, fligner_detection, mood_detection]:
            # detect(t,x) returns a tuple (points,name)
            test_preds += [detect(t, x)]
        prediction += [test_preds]

    visu.detection_plot(t, X, Y, prediction,
                        "detection results (sigma={})".format(noise))
