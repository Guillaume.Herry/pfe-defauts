import random
import numpy as np
import pandas as pd

from scipy import signal
from scipy import stats
import matplotlib.pyplot as plt

#########################################
## mask test for threshold applciation ##
#########################################
"""
n=12
t=np.array([i for i in range(n)])
L=100*t

th_mask=np.array([bool(random.getrandbits(1)) for i in range(n)])
diff_mask=np.diff(th_mask,n=1)

indices = np.nonzero(diff_mask)[0] + 1

print(L, th_mask)

t_bits=np.split(t,indices)
L_bits=np.split(L,indices)

print(L_bits)

min_points=[]
max_points=[]
for t_bit, L_bit in zip(t_bits, L_bits):
    if 1 <= len(L_bit):
        min_points += [ t_bit[np.argmin(L_bit)] ]
        max_points += [ t_bit[np.argmax(L_bit)] ]

print(min_points, max_points)
"""
############################
## scipy correlation test ##
############################
"""
x = np.arange(128) / 128
sig = np.sin(2 * np.pi * x)
sig_noise= np.sin(2 * np.pi * x)
sig_noise[20:40]=0
corr = signal.correlate(sig_noise, sig)
lags = signal.correlation_lags(len(sig), len(sig_noise))
corr /= np.max(corr)

fig, (ax_orig, ax_noise, ax_corr) = plt.subplots(3, 1, figsize=(4.8, 4.8))
ax_orig.plot(sig)
ax_orig.set_title('Original signal')
ax_orig.set_xlabel('Sample Number')
ax_noise.plot(sig_noise)
ax_noise.set_title('Signal with noise')
ax_noise.set_xlabel('Sample Number')
ax_corr.plot(lags, corr)
ax_corr.set_title('Cross-correlated signal')
ax_corr.set_xlabel('Lag')
ax_orig.margins(0, 0.1)
ax_noise.margins(0, 0.1)
ax_corr.margins(0, 0.1)
fig.tight_layout()
plt.show()
"""
############################
## local correlation test ##
############################

def test_cross_correlation(x1, x2, incr=1, corr_length=5):
    ''' Cross corrélation appliquée entre 1-D array x1 et 1-D array x2 sur une portion de longeur corr_length
    '''
    i = 0
    res = np.ones((len(x1), 1))
    while i*incr+corr_length <= len(x1):
        begin = i*incr
        x1_tronc = x1[begin:begin+corr_length]
        x2_tronc = x2[begin:begin+corr_length]
        corr = signal.correlate(x1_tronc,x2_tronc,mode="full")
        print("at {}, correlation is {}".format(i, corr))
        res[begin+corr_length//2] = np.average(corr)
        i += 1
    return res

def test_cross_mannwhitney(x1, x2, incr=1, corr_length=5):
    ''' test de mann_whitney appliquée entre 1-D array x1 et 1-D array x2 sur une portion de longeur corr_length
    '''
    i = 0
    res = np.ones((len(x1), 1))
    while i*incr+corr_length <= len(x1):
        begin = i*incr
        x1_tronc = x1[begin:begin+corr_length]
        x2_tronc = x2[begin:begin+corr_length]
        #corr = signal.correlate(x1_tronc,x2_tronc,mode="full")
        U, p = stats.mannwhitneyu(x1_tronc, x2_tronc, alternative=None)
        print("at {}, mannwhitney is [U,p]=[{},{}]".format(i, U, p))
        res[begin+corr_length//2] = p
        i += 1
    return res

x = np.arange(128) / 128
sig = np.sin(2 * np.pi * x)
sig_2= np.sin(2 * np.pi * x)
sig_2[20:40]=0
local_corr = test_cross_correlation(sig,sig_2)

fig, (ax_orig, ax_noise, ax_corr) = plt.subplots(3, 1, figsize=(4.8, 4.8))
ax_orig.plot(sig)
ax_orig.set_title('Sig_1')
ax_orig.set_xlabel('Sample Number')
ax_noise.plot(sig_2)
ax_noise.set_title('Sig_2')
ax_noise.set_xlabel('Sample Number')
ax_corr.plot(local_corr)
ax_corr.set_title('Local Cross-correlation')
ax_corr.set_xlabel('Sample Number')
ax_orig.margins(0, 0.1)
ax_noise.margins(0, 0.1)
ax_corr.margins(0, 0.1)
fig.tight_layout()
plt.show()

##########################
## panda dataframe test ##
##########################
"""
df = pd.read_csv('fichier_mesures.txt', sep="	",
                     parse_dates=True, index_col='time')

start_date = '2015-11-24 22:00:00'  # '2015-11-25 06:00:00'
end_date = None

df = filter_date_data(df, start_date, end_date)
df = simplify_columns(df)

res_df = pd.DataFrame(index=df.index)
print(res_df)
"""