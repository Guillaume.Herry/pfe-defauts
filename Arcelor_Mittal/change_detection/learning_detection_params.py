"""!@file learning_detection_params.py
This module implements optimisation methods to select the best parameters for the detections:
    -> mannwhitney_detection
    -> mood_detection
    -> fligner_detection
"""

import numpy as np
from tqdm import tqdm
import scipy.optimize as optim

import signals as s
import visualisation as visu
import change_detection as detect

def compute_prediction(t,X,detection_list,params_list,progress=False):
    """ returns a 2-D array which term i,j is the prediction of detection j on sample i
                prediction[i][j]=([predicted points],"name of used test & detection")
        
        X               -- a 1-D array of signal samples x
        detection_list  -- a 1-D array of detection methods
        params_list     -- a 1-D array of params for each detection method
    """
    prediction=[]
    # for each sample x:
    for x in tqdm(X, disable=progress):    
        test_preds=[]
        # run all available detection methods:
        for i,detection in enumerate(detection_list):
            th,obs_length=params_list[i]
            test_preds+=[detection(t,x,th,obs_length)] # detect(t,x) returns a tuple (points,name) 
        prediction+=[test_preds]
    return prediction

def prediction_error(Y,prediction,detected_param=200):
    ''' returns a 2-D array which term i,j is the prediction error of detection j on sample i

        NB: prediction with prediction_error superior to detected_param are considered undetected
            their prediction_error is set to detected_param

        Y               -- a 1_D array of targets y[i]=[(point_1,name_1)]
        prediction      -- a 2-D array which term i,j is the prediction of detection j on sample i
                           prediction[i][j]=([predicted points],"name of used test & detection")
        detected_param  -- int min error for a detection to be considered undetected
    '''
    n=len(Y) # nb of samples
    d=len(prediction[0]) # nb of detections
    errors=np.zeros((n,d))
    for i in range(n):
        for j in range(d):
            if 1 <= len(prediction[i][j][0]):
                # erreur is the time difference between earliest detection and t0
                errors[i,j]=min(abs(Y[i][0][0]-prediction[i][j][0][0]), detected_param) # <== error definition
            else:
                # if no detection, erreur is set arbitrarily to detected_param
                errors[i,j]=detected_param
    return errors

def prediction_stats(prediction_error, success_param=25, detected_param=200):
    ''' returns a 1-D array success rates predictions for each detection method
                a 1-D array undetected rates for each detection method. Undetected iif error equals max_error value.
                a 1-D array average prediction errors for each detection method (only detection are considered)

        prediction_error    -- 2-D array which term i,j is the prediction error of detection j on sample i
        success_param       -- int max error for a detection to be considered a success
        detected_param      -- int min error for a detection to be considered undetected   
    '''
    nb_samples,nb_detections=prediction_error.shape
    success=np.zeros(nb_detections)
    avg=np.zeros(nb_detections) # only detections are considered for the average
    undetected=np.zeros(nb_detections)
    # for each prediction_method
    for j in range(nb_detections):
        # calculate the success rate:
        success[j] = (prediction_error[:,j]<success_param).sum() / nb_samples * 100
        # calculate the average prediction error:
        I = (prediction_error[:,j]<detected_param) # detection mask
        if I.any():
            avg[j] = np.average(prediction_error[I,j])
        # calculate the undetected rate:
        undetected[j] = (1 - I.sum() / nb_samples) * 100

    return success, undetected, avg,

def display_stats(detection_list, signal_collection, prediction_stats, verbose=False, success_param=25, detected_param=200):
    ''' returns a string diplaying the success_rate information

        detection_list      -- a 1-D array of the used detection methods
        signal_collection   -- a 1-D array of the used signal collection
        prediction_stats    -- a 3 element tuple composed of:
            -success_rate   -- a 1-D array percentages of successful predictions for each detection method
            -avg_error      -- a 1-D array average prediction errors for each detection method (only detection are considered)
            -undetect_rate    -- a 1-D array number of undetected samples for each detection method. Undetected iif error equals max_error value
    '''
    # collect the information to be displayed from the prediction_stats tuple:
    success_rate, undetect_rate, avg_detect_error = prediction_stats
    signal_collection_str = [signal.__name__ for signal in signal_collection]
    
    msg = "On {} signals:\n".format(", ".join(signal_collection_str))
    for i,detection in enumerate(detection_list):
        msg_1 = detection.__name__
        msg_2 = "show {:>3.0f}%".format(success_rate[i])
        msg_3 = " of success"
        if verbose:
            msg_31 = " ({:>3.0f}% samples were not detected, ".format(undetect_rate[i])
            msg_32 = "the average detection error was {:^5.1f}).".format(avg_detect_error[i])
            msg_3+= msg_31 + msg_32
        else:
            msg_3+="."
        # assemble all three messages respecting a tabular alignment: 
        msg+= " -> " + "{:<22}{:<9}{}".format(msg_1, msg_2, msg_3) + "\n"
    
    print(msg)
    return msg
    
def learn_parameters(t,nb_samples):
    """ returns a 1-D of learnt parameters (error,(threshold,n_obs)) for each detection method

        nb_samples  -- int number of samples to generate for training
    """

    # detection methods and corresponding params to be optimized:
    detection_list=[detect.mannwhitney_detection, detect.fligner_detection, detect.mood_detection]    
    init_params_list = [(0.2,50),(0.001,50),(0.001,50)]
    bound_params_list = [[(0.05,0.4),(20,80)],[(0.0001,0.01),(3,80)],[(0.0001,0.01),(3,80)]]
    # corresponding signal collections for learning:
    signal_collection_list=[[s.step_rectangle,s.ramp],[s.step_rectangle,s.ramp],[s.noise_change]]
    
    learnt_params=[]
    for i,detection in enumerate(tqdm(detection_list)):

        # generate nb_samples signals from the corresponding signal collection
        X,Y=s.signal_samples(t,signal_collection_list[i],nb_samples=nb_samples)
        
        # Define the target function:
        def target (params):
            prediction = compute_prediction(t,X,[detection],[params],progress=True)
            pred_error = prediction_error(Y,prediction,detected_param=200)
            emp_risk = np.sum(pred_error, axis=0) / nb_samples
            return emp_risk[0]

        # Set bounds for params:
        th_min, th_max = bound_params_list[i][0]
        obs_min, obs_max = bound_params_list[i][1]

        # Define the bounds on th (first column) and obs_length (second column)
        bounds = optim.Bounds(
        np.array([th_min, obs_min]),
        np.array([th_max, obs_max])
        )

        # Run the optimization
        res = optim.minimize(target, init_params_list[i],method='Powell',
        bounds=bounds,options={'xtol': 5, 'disp': False})
        
        # Add the error and found parameters:
        learnt_params+=[(res.fun, res.x)]
    
    return learnt_params


if __name__ == '__main__':
    
    t = np.linspace(-100, 100, 500)

    # #########################################################
    # # Detection performance with chosen parameters (th,obs) #
    # #########################################################

    # available detection methods and corresponding params_list (th,obs):
    detection_list=[detect.mannwhitney_detection, detect.fligner_detection, detect.mood_detection]
    params_list=[(0.1836881,42),(0.006,68),(0.005,65)]
    # available signal_collections:
    signal_collection_list=[[s.step_rectangle],[s.ramp],[s.noise_change]]
    
    # for each signal_collection we want to test to detection method performance:
    for signal_collection in signal_collection_list:
        
        # generate nb_samples signals from the chosen signal_collection
        X,Y=s.signal_samples(t,signal_collection,nb_samples=100)

        # run all available detection methods:
        prediction=compute_prediction(t,X,detection_list,params_list)
        
        # display performance:
        #visu.detection_plot(t,X,Y,prediction,"detection results")

        # mesure prediction error and performance statistics:
        pred_error = prediction_error(Y,prediction)
        pred_stats = prediction_stats(pred_error)
        
        # display statistics:
        display_stats(detection_list, signal_collection, pred_stats, verbose=False)

    # #####################################################
    # # Finding the optimal detection parameters (th,obs) #
    # #####################################################
    """
    learnt_params=learn_parameters(t,nb_samples=100)
    print(learnt_params)

    # [(17.169961053630413, array([ 0.1836881 , 49.15602897])), 
    # (76.89419767822865, array([2.83654249e-03, 6.18324738e+01])), 
    # (80.88322211056698, array([3.88146351e-03, 6.34894324e+01]))]

    opt_params_list = [(0.1836881,42),(0.003,68),(0.004,65)]
    """

