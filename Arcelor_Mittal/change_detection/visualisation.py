"""!@file visualisation.py
This module implements convenient plot functions:
    -> to plot statistical tests (test_plot)
    -> to plot detections (detection_plot)
"""

from matplotlib import pyplot as plt
import math

def function_plot(t,x):
    ''' Plot une fonction f '''
    plt.figure()
    plt.plot(t, x, '-b', linewidth=1)
    plt.show()

def test_plot(t,x,test_result,title,nb_to_plot=5,nb_lignes=None,nb_colonnes=1):
    """ Re-formats the statistical test output (x,[(points_1,name_1),(points_2,name_2)])
        in order to call functions_plot

        x           -- 1-D array representing the signal
        test_result -- 1-D array representing the labels
        prediction  -- 1-D array representing the predicted labels
        title       -- str title
        nb_to_plot  -- int number of elements to print 
    """
    nb_to_plot=min(len(x),nb_to_plot)
    plot_list=[]
    for i in range(nb_to_plot):
        plot_list+=[(0,x[i][0]),(1,test_result[i])]
    functions_plot(t,plot_list,title,nb_lignes=nb_lignes,nb_colonnes=nb_colonnes)

def detection_plot(t,x,y,prediction,title,nb_to_plot=10,nb_lignes=None,nb_colonnes=1):
    """ Re-formats the detection prediction output (x,[(points_1,name_1),(points_2,name_2)])
        in order to call functions_plot

        x           -- 1-D array representing the signal
        y           -- 1-D array representing the labels
        prediction  -- 1-D array representing the predicted labels
        title       -- str title
        nb_to_plot  -- int number of elements to print 
    """
    nb_to_plot=min(len(x),nb_to_plot)
    plot_list=[]
    for i in range(nb_to_plot):
        plot_list+=[(2,(x[i],y[i]+prediction[i]))]
    functions_plot(t,plot_list,title,nb_lignes=nb_lignes,nb_colonnes=nb_colonnes)

def functions_plot(t,L,title,nb_lignes=None,nb_colonnes=1):
    ''' Plot une liste de 1-D arrays sous la forme d'un tableau nb_lignes*nb_colonnes 
    
        t           -- 1-D array des temps
        L           -- liste de couple (type d'affichage,valeur à afficher)
        title       -- titre de la figure
        nb_lignes   -- nombre de ligne de l'affichage
        nb_colonnes -- nombre de colonnes de l'affichage
    '''
    
    # 1/ vérification des dimensions d'affichages:
    if nb_lignes is None:
        nb_lignes=math.ceil(len(L)/nb_colonnes) # partie entière supérieure

    if nb_lignes*nb_colonnes < len(L):
        raise NameError('Nombre de lignes*colonnes trop petit pour afficher {} graphs'.format(len(L)))
   
    # 2/ on réalise le plot:
    else:
        fig=plt.figure()
        
        # on parcourt la liste de fonctions à plot
        for i, l in enumerate(L, start=1):
            ax1 = fig.add_subplot(nb_lignes, nb_colonnes, i)
            
            # s'il s'agit d'un affichage de type 0 correspondant à un signal,
            #  e.g. f(t) = ramp: 
            if l[0] == 0:
                ax1.plot(t, l[1], '-b', linewidth=1)
            
            # s'il s'agit d'un affichage de type 1 correspondant à un test,
            #  e.g. f(t) = (test, p-value):
            elif l[0] == 1:
                color = 'tab:blue'
                ax1.set_ylabel('test', color=color)
                ax1.plot(t, l[1][:,0], color=color, linewidth=1)
                ax1.tick_params(axis='y', labelcolor=color)
                
                ax2 = ax1.twinx()
                
                color = 'tab:red'
                ax2.set_ylabel('p-value', color=color)
                ax2.plot(t, l[1][:,1], color=color, linewidth=1)
                ax2.tick_params(axis='y', labelcolor=color)
            
            # s'il s'agit d'un affichage de type 2 correspondant à une détection, 
            # e.g. (x,[(points_1,name_1),(points_2,name_2)]):
            elif l[0] == 2:
                ax1.plot(t, l[1][0], '-b', linewidth=1)
                signal_min=min(l[1][0])
                signal_max=max(l[1][0])
                for i,points in enumerate(l[1][1]):
                    color=['g', 'm', 'c', 'y', 'k', 'w'][i%6]
                    if i == 0:
                        ax1.vlines(points[0], signal_min, signal_max, colors='r', linestyles='dashed', label='_'+points[1])
                        ax1.annotate(points[1], (points[0][0]+1, signal_min), color='r', fontsize='xx-small', bbox=dict(facecolor='white', edgecolor='none', pad=0))
                        # ax1.legend(loc='upper left', fontsize='xx-small') # for a legend inside each subplot
                    else:
                        ax1.vlines(points[0], signal_min, signal_max, colors=color, linestyles='dashed', label=points[1])

            # si le type d'affichage n'est pas reconnu:
            else:
                raise NameError("Invalid argument L, display type not recognised")
            
            # add figure the subplots with a title and a legend:
            fig.suptitle("{} :".format(title))
            fig.add_subplot(ax1)
            handles, labels = ax1.get_legend_handles_labels()
            fig.legend(handles, labels, loc='upper left', fontsize='xx-small')
        
        fig.canvas.set_window_title(str(title))
        plt.show()

