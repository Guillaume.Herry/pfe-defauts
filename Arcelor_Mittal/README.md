## Arcelor_Mittal:

*Arcelor_Mittal* implements **statistical change-point detection methods** to predict upcoming defects on production line machinery.

## Structure:

* The *change_detection* directory gathers change-point detection tools and test them on elementary defaults:
    * *signals.py*: enables the generation of simple defects.
    * *statistic_tests.py*: implements statistical tests.
    * *change_detection.py*: integrates detection methods. 
    * *visualisation.py*: allows a graphical representation of the test and detection outputs. 
    * *learning_detection_params.py*: implements a metric to mesure prediction results and optimize detection parameters.
    <br>

* The *Dataset analysis* directory run the *change detection* tools on the real furnace defect provided by the Arcelor Mittal.

