############
## idée 1 ##
############

import numpy as np
from dtw import *

## A noisy sine wave as query
idx = np.linspace(0,6.28,num=100)
query = np.sin(idx) + np.random.uniform(size=100)/10.0

## A cosine is for template; sin and cos are offset by 25 samples
template = np.cos(idx)

## Find the best match with the canonical recursion formula
alignment = dtw(query, template, keep_internals=True)

## Display the warping curve, i.e. the alignment curve
alignment.plot(type="threeway")

## Align and plot with the Rabiner-Juang type VI-c unsmoothed recursion
dtw(query, template, keep_internals=True, step_pattern=rabinerJuangStepPattern(6, "c")).plot(type="twoway",offset=-2)

## See the recursion relation, as formula and diagram
print(rabinerJuangStepPattern(6,"c"))
rabinerJuangStepPattern(6,"c").plot()

## And much more!


############
## idée 2 ##
############

"""
Goal: measure a visual similarity rather than a crude RMS measurement.

I think the best solution to measuring this "phase independent" matching criterion is to first take the fast Fourier transform (FFT) 
of both signals to obtain a moving window of power spectrum values. Then, then in each window, calculate the mean square difference 
between the power levels for each frequency component. Finally, take the sum of these values for each window as your overall difference metric.

NB: If you want to ignore the absolute amplitudes of the two waveforms in your comparison then normalise them first. 
This can be done after the FFT by dividing the calculated FFT power level for each frequency by the average of all the power values. 
(or if done in in the time domain, you can simply divide the amplitude of each waveform by its RMS value in each window)

This will give you a time varying metric that compares the shape and size of the waveforms that is not sensitive to slow changes 
in phase between the waveforms that cause the peaks to drift about.

"""