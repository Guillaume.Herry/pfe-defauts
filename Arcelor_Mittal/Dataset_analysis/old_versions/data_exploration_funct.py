import pandas as pd


def simplify_columns(df):
    '''
    This function is used to simplify the names of the columns of the Dataframe.
    With this we only keep the usefull information

    '''

    list_columns = df.columns
    simplified_columns = [column.split(" ")[-1] + " " + column.split(" ")[0] + (
        " - " + column.split(" ")[1] if column.split(" ")[1] != "en" else "") for column in list_columns]
    df.columns = simplified_columns
    return df


def get_delta(df, sensor):
    '''

    This function is used to plot the delta between consigne and mesure of one sensor

    '''
    list_col = df.columns
    # print(list_col)
    consigne_name = [col_name for col_name in list_col if (
        sensor in col_name and 'Consigne' in col_name)]
    mesure_name = [col_name for col_name in list_col if (
        sensor in col_name and 'Mesure' in col_name)]
    if len(consigne_name) > 0 and len(mesure_name) > 0:
        return df[mesure_name[0]]-df[consigne_name[0]], True

    # if there is no consigne/mesure
    else:
        print(sensor + ' no mesure or consigne')
        return False, False


def filter_date_data(df, start_date=None, end_date=None):
    '''

    This function is used to slice the time index of a dataframe based on a start and/or an end date
    '''
    if start_date:
        df = df.loc[start_date:]
    if end_date:
        df = df.loc[:end_date]

    return df
