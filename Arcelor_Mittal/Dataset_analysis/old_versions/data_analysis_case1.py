from EDA_functions import *

if __name__ == '__main__':

    ##########################
    ### DATA PREPROCESSING ###
    ##########################

    df = pd.read_csv('fichier_mesures.txt', sep="	",
                     parse_dates=True, index_col='time')

    start_date = '2015-11-24 22:00:00'  # '2015-11-25 06:00:00'
    end_date = None

    df = filter_date_data(df, start_date, end_date)
    df = simplify_columns(df)

    list_tests = [test_stat.mannwhitney, test_stat.fligner]

    set_sensor = set([name.split(" ")[0] for name in df.columns])
    # dictionnary with all the mesures related to one sensor
    dict_sensor = {}
    list_col = df.columns
    for sensor in tqdm(set_sensor):
        dict_sensor[sensor] = [
            col_name for col_name in list_col if sensor in col_name]

    # Create a DataFrame with all the delta for each sensor
    delta_df = pd.DataFrame(index=df.index)

    L = []
    col = []
    for sensor in tqdm(set_sensor):
        Delta, Bool = get_delta(df, sensor)
        if Bool:
            L.append(Delta)
            col.append('delta ' + sensor)

    delta_df = pd.concat(L, axis=1, ignore_index=False)
    delta_df.columns = col
    # print(delta_df["delta TIC0200"].head())

    print('Starting plots')

##########################
###   PLOTS OF TESTS   ###
##########################

    DF_to_plot = delta_df[["delta TIC0300"]]
    t = DF_to_plot.index
    x = DF_to_plot.values
    print(len(t))
    fig, axes = plt.subplots(nrows=3, ncols=len(list_tests))
    #fig1, axes1 = plt.subplots(nrows=2, ncols=len(list_tests))

    for i, test in tqdm(enumerate(list_tests)):
        res = np.array(test(x, obs_length=1000, max_p_value=1, incr=100))

        print('len rez', len(res[:, 0]))

        # print(res)
        DF_to_plot['res_0'] = res[:, 0]
        DF_to_plot['res_1'] = res[:, 1]
        DF_to_plot['x'] = DF_to_plot.index

        DF_to_plot_res = DF_to_plot[DF_to_plot['res_0'] != 1]
        t_res = DF_to_plot.index
        print(DF_to_plot_res.info())
        DF_to_plot.plot(y="delta TIC0300",
                        ax=axes[0, i], title='delta TIC0300')
        DF_to_plot_res.plot(x='x', y='res_0',
                            ax=axes[1, i], title='statistic '+test.__name__)
        DF_to_plot_res.plot(x='x', y='res_1',
                            ax=axes[2, i], title='p-value '+test.__name__)

        print('loop for {} finished'.format(test.__name__))

        Rolling = DF_to_plot_res['res_0'].rolling(5).sum()
        print(Rolling.head(30))
        threshold = 10000

        # DF_to_plot_res.plot.scatter(x='x', y='res_0',
        #                             ax=axes1[0, i], title=test.__name__, marker='x')

        # DF_to_plot_res.plot(x='x', y='res_0',
        #                    ax=axes1[0, i], title=test.__name__)
        filteredRolling = DF_to_plot_res.mask(Rolling <
                                              threshold)

        #    (Rolling.values > 1-threshold) | (Rolling.values < threshold))

        # filteredRolling.plot.scatter(
        #   x='x', y='res_0', ax=axes1[1, i], marker='x')

    plt.tight_layout()
    plt.show()
