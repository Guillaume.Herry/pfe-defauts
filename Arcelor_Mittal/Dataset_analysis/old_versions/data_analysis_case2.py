
from EDA_functions import *


if __name__ == '__main__':

    ##########################
    ### DATA PREPROCESSING ###
    ##########################

    df = pd.read_csv('fichier_mesures.txt', sep="	",
                     parse_dates=True, index_col='time')

    start_date = '2015-11-24 22:00:00'  # '2015-11-25 06:00:00'
    end_date = None

    df = filter_date_data(df, start_date, end_date)
    df = simplify_columns(df)

    list_tests = [test_stat.mannwhitney, test_stat.fligner]

    set_sensor = set([name.split(" ")[0] for name in df.columns])
    # dictionnary with all the mesures related to one sensor
    dict_sensor = {}
    list_col = df.columns
    for sensor in set_sensor:
        dict_sensor[sensor] = [
            col_name for col_name in list_col if sensor in col_name]

    # Create a DataFrame with all the delta for each sensor
    delta_df = pd.DataFrame(index=df.index)

    L = []
    col = []
    for sensor in set_sensor:
        Delta, Bool = get_delta(df, sensor)
        if Bool:
            L.append(Delta)
            col.append('delta ' + sensor)

    delta_df = pd.concat(L, axis=1, ignore_index=False)
    delta_df.columns = col
    # print(delta_df["delta TIC0200"].head())


##########################
###   PLOTS OF TESTS   ###
##########################

    DF_to_plot = delta_df[["delta TIC0300"]]
    t = DF_to_plot.index
    x = DF_to_plot.values

    for i, test in enumerate(list_tests):
        res = np.array(test(x, obs_length=50, max_p_value=0.001))

        # print(res)
        DF_to_plot['res_0'] = res[:, 0]
        DF_to_plot['res_1'] = res[:, 1]

    #     DF_to_plot.plot(y="delta TIC0400", ax=axes[0, i])
    #     DF_to_plot.plot(y='res_0', ax=axes[1, i], title=test.__name__)
    #     DF_to_plot.plot(y='res_1', ax=axes[2, i], title=test.__name__)

    #     print('loop for {} finished'.format(test.__name__))

    # plt.tight_layout()
    # plt.show()

    #change_d.mannwhitney_detection(t, x, th=0.2, obs_length=50)


#####
    #change_d.mannwhitney_detection(t, x, th=0.2, obs_length=50)

    # detect(t,x) returns a tuple (points,name)
    # test_preds = [change_d.mannwhitney_detection(t, x)]
    # prediction = [test_preds]

    # visu.detection_plot(t, x, x, prediction,
    #                     "detection results ")
    # detect(t,x) returns a tuple (points,name)

    # t = DF_to_plot.index
    # new_t = [i for i in range(len(t))]
    # x = DF_to_plot.values

    # X = [x]
    # test_preds = [change_d.mannwhitney_detection(t, x)]
    # prediction = [test_preds]
    # result = change_d.mannwhitney_detection(new_t, X, th=0.2, obs_length=50)
    # visu.detection_plot(new_t, X, X, prediction)
    # plt.plot(result)
    # plt.show()

        detection = change_d.min_detection

        points = detection(t, res)

        print_output = [(2, (x, [(points, "")]))]

        print(points)
        Y = [1]*len(points)

        plt.scatter(points, Y, marker='x')

        plt.title('Dates detected by min_detection function')
        plt.show()

    # new_t = [i for i in range(len(t))]
    # visu.functions_plot(new_t, print_output, 'TEST')
