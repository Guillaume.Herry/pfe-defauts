from EDA_functions import *
import change_detection_AR as change_d_AR  # nopep8
import numpy


if __name__ == '__main__':

    ##########################
    ### DATA PREPROCESSING ###
    ##########################

    df = pd.read_csv('fichier_mesures.txt', sep="	",
                     parse_dates=True, index_col='time')

    start_date = '2015-11-24 22:00:00'  # '2015-11-25 06:00:00'
    end_date = None

    df = filter_date_data(df, start_date, end_date)
    df = simplify_columns(df)

    list_tests = [change_d_AR.AR_calculation]

    set_sensor = set([name.split(" ")[0] for name in df.columns])

    # dictionnary with all the mesures related to one sensor
    dict_sensor = {}
    list_col = df.columns
    for sensor in set_sensor:
        dict_sensor[sensor] = [
            col_name for col_name in list_col if sensor in col_name]

    # Create a DataFrame with all the delta for each sensor
    delta_df = pd.DataFrame(index=df.index)

    L = []
    col = []
    for sensor in set_sensor:
        Delta, Bool = get_delta(df, sensor)
        if Bool:
            L.append(Delta)
            col.append('delta ' + sensor)

    delta_df = pd.concat(L, axis=1, ignore_index=False)
    delta_df.columns = col
    # print(delta_df["delta TIC0200"].head())


##########################
###   PLOTS OF TESTS   ###
##########################

    DF_to_plot = delta_df[["delta TIC0300"]]
    t = DF_to_plot.index
    x = DF_to_plot.values

    print('start AR')

    # Calculate the AR modelisation and the brand test for the signal
    res_AR = change_d_AR.AR_calculation(x, incr=5, obs_length=100)

    print(len(res_AR))

    # Plot of the values
    fig, axs = plt.subplots(2)
    axs[0].plot(t, x)
    axs[1].plot(t, np.array(res_AR))
    plt.show()
