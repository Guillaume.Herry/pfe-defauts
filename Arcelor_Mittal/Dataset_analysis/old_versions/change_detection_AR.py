import numpy as np
import math

from matplotlib import pyplot as plt
import numpy as np
import scipy.stats
from scipy.ndimage import uniform_filter1d

import nitime
import audiolazy


import signals as s

# steps to code
# 1 Faire la moyenne de deux fenêtres d'analyse larges de W échantillons

# comparer la valeur des moyennes


# def mean_comparison(t, x, incr=1, obs_length=10):

#     res = np.ones((len(x), 2))
#     while i*incr+2*obs_length+1 <= len(x)

#     n = len(t)


def moving_average(x, obs_length=10, max_p_value=1):
    ''' Returns the moving average calculated on moy_length'''
    return uniform_filter1d(x, size=obs_length)


# calculer modele AR sur deux fenetres glissante de taille W
# calcul de modele AR sur la fenetre globale des deux fenetres précédentes

# Pour améliorer le caractère autorégressif on soustrait la moyenne aux signaux avant de les calculer


def AR_calculation(x, incr=1, obs_length=20):
    '''
        This function computes the AR modelisation on 3 differents moving windows

        Window A (First half of the window)
        Window B (Second half of the window)
        Window C (Full window)

        Then the Brandt Test is applied to the these three windows in order to calculate v
    '''
    obs_length = int(obs_length)
    i = 0

    res = np.ones(len(x))
    while i*incr+2*obs_length+1 <= len(x):
        begin = i*incr

        # Creation of the three windows
        a = x[begin:begin+obs_length]
        b = x[begin+obs_length+1:begin+2*obs_length+1]
        c = x[begin:begin+2*obs_length+1]

        a = a-np.mean(a)
        b = b-np.mean(b)
        c = c-np.mean(c)

        # Simulation for the 3 windows
        [ak, sig_sq2] = nitime.algorithms.autoregressive.AR_est_LD(a, 2, rxx=None)
        [ak, sig_sq3] = nitime.algorithms.autoregressive.AR_est_LD(b, 2, rxx=None)
        [ak, sig_sq1] = nitime.algorithms.autoregressive.AR_est_LD(c, 2, rxx=None)

        N = len(a)

        # added abs to prevent error
        print(sig_sq1, sig_sq2, sig_sq3)
        v = abs(2*N*np.log(math.sqrt(sig_sq1))-N *np.log(math.sqrt(sig_sq2))-N*np.log(math.sqrt(sig_sq3)))

        res[begin+obs_length:begin+obs_length+incr] = v
        i += 1

    # constant padding:
    res[0:obs_length]=res[obs_length]
    res[len(x)-obs_length:len(x)]=res[len(x)-obs_length-1]

    return res


def AR_calculation_2(x, incr=1, obs_length=20):
    '''
        This function computes the AR modelisation on 3 differents moving windows

        Window A (First half of the window)
        Window B (Second half of the window)
        Window C (Full window)

        Then the Brandt Test is applied to the these three windows in order to calculate v




    '''
    obs_length = int(obs_length)
    i = 0
    #x = x-np.mean(x)
    sig_list = []

    res = np.ones((len(x)))
    while i*incr+2*obs_length+1 <= len(x):
        begin = i*incr

        # Creation of the three windows
        a = x[begin:begin+obs_length]
        b = x[begin+obs_length+1:begin+2*obs_length+1]
        c = x[begin:begin+2*obs_length+1]

        # mean = np.mean(c)

        a = a-np.mean(a)
        b = b-np.mean(b)
        c = c-np.mean(c)

        acdata1 = audiolazy.acorr(c)
        ldfilt1 = audiolazy.levinson_durbin(acdata1, 2)
        sig_sq1 = ldfilt1.error

        acdata2 = audiolazy.acorr(a)
        ldfilt2 = audiolazy.levinson_durbin(acdata2, 2)
        sig_sq2 = ldfilt2.error

        acdata3 = audiolazy.acorr(b)
        ldfilt3 = audiolazy.levinson_durbin(acdata3, 2)
        sig_sq3 = ldfilt3.error

        N = len(c)

        # added abs to prevent error
        v = abs(2*N*np.log(math.sqrt(abs(sig_sq1)))-N *
                np.log(math.sqrt(abs(sig_sq2)))-N*np.log(math.sqrt(abs(sig_sq3))))
        # print(sig_sq1, sig_sq2, sig_sq3)
        # v = abs(2*N*np.log(math.sqrt(sig_sq1))-N *
        #         np.log(math.sqrt(sig_sq2))-N*np.log(math.sqrt(sig_sq3)))

        # sig_list.append(v)
        res[begin+obs_length] = v
        #print(i, [sig_sq1, sig_sq2, sig_sq3, v])
        i += 1
    return res


if __name__ == '__main__':

    # 1/ Liste de défaults élémentaires (const, step, rampe, intermittent)
    t = np.linspace(-100, 100, 5000)
    defauts = [s.noise_change(t), s.step_rectangle(t),
               s.ramp(t), s.intermittent(t)]

    # Adding noise to the signals
    noise = s.noise_func(0, 0.05)
    defauts_noise_simple = [s.add_noise(t, x, noise) for x in defauts]

    [ak1, sig_sq1] = nitime.algorithms.autoregressive.AR_est_LD(
        defauts[2][0], 2, rxx=None)
    acdata1 = audiolazy.acorr(defauts[2][0])

    print(sig_sq1)

    ldfilt1 = audiolazy.levinson_durbin(acdata1, 5)
    sig_sq1 = ldfilt1.error

    [ak2, sig_sq2] = nitime.algorithms.autoregressive.AR_est_LD(
        defauts[2][0][:250], 2, rxx=None)

    [ak3, sig_sq3] = nitime.algorithms.autoregressive.AR_est_LD(
        defauts[2][0][250:], 2, rxx=None)

    N = 500

    # Calculation of the list of the coefficients for the Brand test
    list_v = AR_calculation(defauts_noise_simple[2][0])

    list_v_2 = AR_calculation_2(defauts_noise_simple[2][0])
    print(len(list_v))

    # Plot of the signal and the results
    fig, axs = plt.subplots(3)
    axs[0].plot(t, defauts_noise_simple[2][0])
    axs[1].plot(np.array(list_v))
    axs[2].plot(np.array(list_v_2))
    # fig, axs=plt.subplots(3)
    # axs[0].plot(defauts[2][0])

    # axs[1].plot(acdata1)
    # axs[2].plot(ak1)

    plt.show()
