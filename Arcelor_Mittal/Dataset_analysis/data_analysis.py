import statistic_tests as test_stat
import numpy as np
import pandas as pd

# util imports:
import os
import sys
import matplotlib.pyplot as plt
from tqdm import tqdm

# local imports:
import util

cwd = os.getcwd()
sys.path.insert(1,  cwd + "/../change_detection")

if __name__ == '__main__':

    ########################
    ## DATA PREPROCESSING ##
    ########################
    print('starting preprocessing \n')

    df = pd.read_csv('fichier_mesures.txt', sep="	",
                     parse_dates=True, index_col='time')

    start_date = '2015-11-24 22:00:00'  # '2015-11-25 06:00:00'
    end_date = None

    df = util.filter_date_data(df, start_date, end_date)
    df = util.simplify_columns(df)

    ########################
    ### Sensor Selection ###
    ########################
    print('starting sensor selection \n')

    set_sensor = set([name.split(" ")[0] for name in df.columns])
    # print(set_sensor)
    """
    {'FT0502', 'TIC0300', 'TIC0200', 'FT0801', 'TIC0600', 'FT0101', 'FT0501', 'FIC0802', 'FIC0702', 'TIC0400', 'FT0202', 
    'FIC0202', 'FT0102', 'FIC0501', 'FT0302', 'TT0701', 'FIC0201', 'FIC0102', 'FIC0602', 'TIC0700', 'FT0201', 'TIC0500', 
    'FT0702', 'FIC0701', 'FIC0402', 'FT0601', 'FT0402', 'FIC0502', 'TIC0100', 'FT0602', 'FT0401', 'FIC0101', 'FT0301', 'PT1601', 
    'FIC0601', 'FIC0302', 'FT0701', 'FIC0301', 'TT0802', 'FIC0801', 'FIC0401', 'PIC1601', 'TIC0800'}
    """

    set_sensor = {'TIC0300'}
    print("selected sensors: {}".format(set_sensor))

    # dictionnary with all the measures related to one sensor
    dict_sensor = {}
    list_col = df.columns
    for sensor in set_sensor:
        dict_sensor[sensor] = [
            col_name for col_name in list_col if sensor in col_name]

    print("sensors and their available measures: {}".format(dict_sensor))
    # {'TIC0300': ['TIC0300 Mesure - température', 'TIC0300 Consigne']}

    #######################
    ## Delta Calculation ##
    #######################
    print('starting Delta calculation \n')

    L = []
    col = []
    for sensor in set_sensor:
        # tries to caculate the delta, returns false if a measure is missing:
        Delta, Bool = util.get_delta(df, sensor)
        if Bool:
            L.append(Delta)
            col.append('delta ' + sensor)

    delta_df = pd.concat(L, axis=1, ignore_index=False)
    delta_df.columns = col

    print(delta_df.head())

    ########################
    ## Tests calculation  ##
    ########################
    print('starting test calculation \n')

    # choix des tests statistiques à effectuer:
    tests = [test_stat.mannwhitney, test_stat.fligner, test_stat.brandt]

    for sensor in set_sensor:

        delta_sensor = "delta " + sensor  # str delta sensor name

        if delta_sensor in delta_df.columns:

            t = delta_df[delta_sensor].index
            x = delta_df[delta_sensor].values

            # preforms tests on signal x:
            test_results = []
            for test in tqdm(tests):
                test_results.append(test(x, incr=50, obs_length=1000))

            ###########################
            ## Result visualisation  ##
            ###########################

            fig, axs = plt.subplots(len(test_results)+2)

            # subplot 1 shows the measures (consigne et capteur):
            for measure in dict_sensor[sensor]:
                axs[0].plot(t, df[measure].values)

            # subplot2 shows the delta:
            color = 'tab:brown'
            axs[1].set_ylabel(delta_sensor, color=color)
            axs[1].plot(t, x, color=color)

            # subplot 3 shows the test results
            for i, test_res in enumerate(test_results):
                color = 'tab:blue'
                axs[i+2].set_ylabel(tests[i].__name__, color=color)
                # statistic value
                axs[i+2].plot(t, test_res[:, 0], color=color)

                ax2 = axs[i+2].twinx()

                color = 'tab:red'
                ax2.set_ylabel('p-value', color=color)
                ax2.plot(t, test_res[:, 1], color=color)  # p-value

            plt.tight_layout()
            plt.show()
