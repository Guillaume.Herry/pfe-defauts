
import util

import numpy as np
import pandas as pd

# util imports:
import os
import sys
import matplotlib.pyplot as plt
from tqdm import tqdm
cwd = os.getcwd()  # nopep8
sys.path.insert(1,  cwd + "/../change_detection")  # nopep8

# local imports:
import statistic_tests as test_stat
import change_detection as change_d


def gaussian(x, mu, sig):
    return np.exp(-np.power(x - mu, 2.) / (2 * np.power(sig, 2.)))


if __name__ == '__main__':

    ########################
    ## DATA PREPROCESSING ##
    ########################
    print('starting preprocessing \n')

    df = pd.read_csv('fichier_mesures.txt', sep="	",
                     parse_dates=True, index_col='time')

    start_date = '2015-11-24 22:00:00'  # '2015-11-25 06:00:00'
    end_date = None

    df = util.filter_date_data(df, start_date, end_date)
    df = util.simplify_columns(df)

    ########################
    ### Sensor Selection ###
    ########################
    print('starting sensor selection \n')

    set_sensor = set([name.split(" ")[0] for name in df.columns])
    # print(set_sensor)
    """
    {'FT0502', 'TIC0300', 'TIC0200', 'FT0801', 'TIC0600', 'FT0101', 'FT0501', 'FIC0802', 'FIC0702', 'TIC0400', 'FT0202',
    'FIC0202', 'FT0102', 'FIC0501', 'FT0302', 'TT0701', 'FIC0201', 'FIC0102', 'FIC0602', 'TIC0700', 'FT0201', 'TIC0500',
    'FT0702', 'FIC0701', 'FIC0402', 'FT0601', 'FT0402', 'FIC0502', 'TIC0100', 'FT0602', 'FT0401', 'FIC0101', 'FT0301', 'PT1601',
    'FIC0601', 'FIC0302', 'FT0701', 'FIC0301', 'TT0802', 'FIC0801', 'FIC0401', 'PIC1601', 'TIC0800'}
    """

    #set_sensor = {'TIC0300'}
    print("selected sensors: {}".format(set_sensor))

    # dictionnary with all the measures related to one sensor
    dict_sensor = {}
    list_col = df.columns
    for sensor in set_sensor:
        dict_sensor[sensor] = [
            col_name for col_name in list_col if sensor in col_name]

    print("sensors and their available measures: {}".format(dict_sensor))
    # {'TIC0300': ['TIC0300 Mesure - température', 'TIC0300 Consigne']}

    #######################
    ## Delta Calculation ##
    #######################
    print('starting Delta calculation \n')

    L = []
    col = []
    for sensor in set_sensor:
        # tries to caculate the delta, returns false if a measure is missing:
        Delta, Bool = util.get_delta(df, sensor)
        if Bool:
            L.append(Delta)
            col.append('delta ' + sensor)

    delta_df = pd.concat(L, axis=1, ignore_index=False)
    delta_df.columns = col

    print(delta_df.head())

    ###########################
    ## Definition of params  ##
    ###########################

    offset = 3000
    sensor_threshold = 0.4
    ########################
    ## Tests calculation  ##
    ########################
    print('starting test calculation \n')

    # choix des tests statistiques à effectuer:
    #test_stat.mannwhitney, test_stat.fligner,
    tests = [test_stat.brandt]
    #detected = np.zeros(len(delta_df.index))
    detected_gaussian = np.zeros(len(delta_df.index))

    for sensor in set_sensor:

        delta_sensor = "delta " + sensor  # str delta sensor name
        if delta_sensor in delta_df.columns:

            t = delta_df[delta_sensor].index
            x = delta_df[delta_sensor].values

            # preforms tests on signal x:
            test_results = []
            for test in tqdm(tests):
                test_results.append(test(x, incr=50, obs_length=1000))

            result_brandt = np.array(test_results[-1])
            result_brandt[:, 1] = result_brandt[:, 0]
            points = change_d.max_detection(t, result_brandt, 4000)
            for point in points:
                for i in range(-offset, offset):
                    #detected[point+i] += 10
                    detected_gaussian[point+i] += 1 * gaussian(i, 0, offset/3)

            ###########################
            ## Result visualisation  ##
            ###########################

            print(points)

        # plt.show()
    error_timer = df.index[np.argmax(detected_gaussian)]

    plt.plot(df.index, detected_gaussian, label='Error detection')

    if max(detected_gaussian) >= sensor_threshold*10*len(delta_df.columns)*2:
        print(
            f'{error_timer} error detected using {len(delta_df.columns)*2} sensors')
        plt.plot([error_timer], [max(detected_gaussian)],
                 marker='x', markersize=3, color="red", label=f'Detected error at {error_timer}')
    plt.legend(loc='best')
    plt.title(
        'Changepoints determined by sensors using delta of measures')
    plt.tight_layout()
    plt.show()
