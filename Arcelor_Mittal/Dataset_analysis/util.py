import pandas as pd
import numpy as np

from scipy import signal


def simplify_columns(df):
    '''
    This function is used to simplify the names of the columns of the Dataframe.
    With this we only keep the useful information

    It only keeps Mesure type: Consigne/Mesure, sensor name and the measure quantity

        df  -- dataframe simplified with the function


    '''

    list_columns = df.columns
    simplified_columns = [column.split(" ")[-1] + " " + column.split(" ")[0] + (
        " - " + column.split(" ")[1] if column.split(" ")[1] != "en" else "") for column in list_columns]
    df.columns = simplified_columns
    return df


def filter_date_data(df, start_date=None, end_date=None):
    '''
    This function is used to slice the time index of a dataframe based on a start and/or an end date

        df      -- pandas dataframe to be sliced
        start_date  -- str corresponding to the desired date to slice on 
        end_date  -- str corresponding to the desired date to slice on 

    '''
    if start_date:
        df = df.loc[start_date:]
    if end_date:
        df = df.loc[:end_date]

    return df


def get_delta(df, sensor):
    '''returns a pandas dataframe series sensor measure - sensor consigne
               a bool (true is there is a measure/consigne match, false if not)

        df      -- pandas dataframe of the sensors
        sensor  -- str corresponding to the sensor name

    '''
    list_col = df.columns

    consigne_name = [col_name for col_name in list_col if (
        sensor in col_name and 'Consigne' in col_name)]
    mesure_name = [col_name for col_name in list_col if (
        sensor in col_name and 'Mesure' in col_name)]

    # if there is a consigne/mesure match:
    if len(consigne_name) > 0 and len(mesure_name) > 0:
        df_res = df[mesure_name[0]]-df[consigne_name[0]]

        # idea 1, low-pass filter on delta:
        sos = signal.butter(4, 0.001, btype='low', analog=False, output='sos')
        res_filtered = signal.sosfilt(sos, df_res.values.tolist())
        # res_normalised=res_filtered/np.max(np.abs(res_filtered))
        df_res_smoothed = pd.Series(res_filtered)

        # idea2, take the gradient to spot sharp changes:
        #df_res_gradient = pd.Series(np.gradient(df_res.values.tolist()))

        return df_res_smoothed, True

    # if there is no consigne or mesure:
    else:
        print(sensor + ' no mesure or consigne')
        return False, False
